from unittest import main, TestCase
from app import app

class UnitTests(TestCase):

    # all charities status code test
    def test_charities(self):
        tester = app.test_client()
        call = tester.get("/charity")
        self.assertEqual(call.status_code, 200)

    # all charities length test
    def test_charities_len(self):
        tester = app.test_client()
        info = (tester.get("/charity")).json
        self.assertEqual(len(info["content"]), 20)

    # charities by id status code test
    def test_charities_id(self):
        tester = app.test_client()
        call = tester.get("/charity/1")
        self.assertEqual(call.status_code, 200)
    
    # charities by id length test
    def test_charities_id_len(self):
        tester = app.test_client()
        info = (tester.get("/charity/1")).json
        self.assertEqual(len(info), 18)

    # all events status code test
    def test_events(self):
        tester = app.test_client()
        call = tester.get("/event")
        self.assertEqual(call.status_code, 200)

    # all events length test
    def test_events_len(self):
        tester = app.test_client()
        info = (tester.get("/event")).json
        self.assertEqual(len(info["content"]), 20)

    # events by id status code test
    def test_events_id(self):
        tester = app.test_client()
        call = tester.get("/event/Z7r9jZ1AdGaus")
        self.assertEqual(call.status_code, 200)

    # events by id length test
    def test_events_id_len(self):
        tester = app.test_client()
        info = (tester.get("/event/Z7r9jZ1AdGaus")).json
        self.assertEqual(len(info), 21)

    # all locations status code test
    def test_locations(self):
        tester = app.test_client()
        call = tester.get("/location")
        self.assertEqual(call.status_code, 200)

    # all locations length test
    def test_locations_len(self):
        tester = app.test_client()
        info = (tester.get("/location")).json
        self.assertEqual(len(info["content"]), 20)

    # locations by id status code test
    def test_locations_id(self):
        tester = app.test_client()
        call = tester.get("/location/1")
        self.assertEqual(call.status_code, 200)

    # locations by id length test
    def test_locations_id_len(self):
        tester = app.test_client()
        info = (tester.get("/location/1")).json
        self.assertEqual(len(info), 18)

if __name__ == "__main__":
    main()
