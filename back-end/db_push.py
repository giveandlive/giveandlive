from cgitb import reset
from app import db
import os
import json
import csv
from models import Charities, Events, Locations
import requests
import base64
import time
import glob

# hidden API keys
from config import api_keys
curr_directory = os.path.dirname(__file__)

# helper function to process city name for edge cases
def setUpCity(city):
    city_split = city.split('.')
    if (len(city_split) > 1):
        city = city_split[1]
    if city == 'Roanake' :
        city = 'Roanoke'
    if city == 'Dulles' :
        city = 'Dulles+Town+Center'
    if city == 'Clinton+Twp' :
        city = 'Clinton'
    if city == 'South-Boston' :
        city = 'Boston'
    if city == 'Over-Town, Miami' :
        city = 'Miami'
    if city_split[0] == 'St' :
        if city == 'petersburg' or city == 'louis':
            city = 'st+' + city.strip()
        else:
            city = 'saint+' + city.strip()
    return city
    
# function to pull data from TicketMaster and push into database
def populate_events():
    # pull all the relevant charity cities
    Events.query.delete()
    db.session.commit()
    db.create_all()
    x = db.session.query(Charities).all()
    cities = dict()
    for i in x :   
        city = i.city
        stateCode = i.state
        print(city)
        cities[city] = stateCode
    
    # pull events for each city
    eventList = []
    # make sure we don't try to push duplicate events
    eventKeys = set()

    for city in cities:
        stateCode = cities[city]
        cityPull = setUpCity(city)
        # construct API endpoint link
        url = "https://app.ticketmaster.com/discovery/v2/events.json?city={}&stateCode={}&apikey={}".format(cityPull, stateCode, api_keys["ticketmaster"])
        response = requests.get(url)
        data  = response.json()
        with open('events.json', 'w') as convert_file:
            convert_file.write(json.dumps(data))
        # make sure the list of event is not empty
        try:
            if data["_embedded"]["events"]:
                for event in data["_embedded"]["events"]:
                    try:
                        id = event["id"]
                        if id in eventKeys:
                            print("go to the next event, this is a duplicate")
                            continue
                        else:
                            eventKeys.add(id)
                    except:
                        id = "NaN"
                    try:
                        name = event["name"]
                    except:
                        name = "NaN"
                    try: 
                        image = event["images"][0]["url"]
                    except:
                        image = "NaN"
                    try: 
                        startDate = event["dates"]["start"]["localDate"]
                    except:
                        startDate = "NaN"
                    try: 
                        startTime = event["dates"]["start"]["localTime"]
                    except:
                        startTime = "NaN"
                    try: 
                        status = event["dates"]["status"]["code"]
                    except:
                        status = "NaN"
                    try: 
                        segment = event["classifications"][0]["segment"]["name"]
                    except:
                        segment = "NaN"
                    try: 
                        genre = event["classifications"][0]["genre"]["name"]
                    except:
                        genre = "NaN"
                    try: 
                        promoter = event["promoter"]["name"]
                    except:
                        promoter = "NaN"
                    try: 
                        priceMin = event["priceRanges"][0]["min"]
                    except:
                        priceMin = -1 #"NaN"
                    try: 
                        priceMax = event["priceRanges"][0]["max"]
                    except:
                        priceMax = -1 #"NaN"
                    try: 
                        info = event["info"]
                    except:
                        info = "NaN"
                    try: 
                        pleaseNote = event["pleaseNote"]
                    except:
                        pleaseNote = "NaN"
                    try: 
                        ticketLimit = event["accessibility"]["ticketLimit"]
                    except:
                        ticketLimit = -1 #"NaN"
                    try: 
                        address = event["_embedded"]["venues"][0]["address"]["line1"]
                    except:
                        address = "NaN"
                    try: 
                        venue = event["_embedded"]["venues"][0]["name"]
                    except:
                        venue = "NaN"
                    try:
                        twitter = event["_embedded"]["attractions"][0]["externalLinks"]["twitter"][0]["url"]
                        #event["_embedded"]["venues"][0]["state"]["stateCode"]
                    except:
                        twitter = "NaN"
                
                
                    print("item:" + event["name"] + "\n")
                    new_event = Events(
                        id = id,
                        name = name,
                        image = image,
                        startDate = startDate,
                        startTime = startTime,
                        status = status,
                        segment = segment,
                        genre = genre,
                        promoter = promoter,
                        priceMin = priceMin,
                        priceMax = priceMax,
                        info = info,
                        pleaseNote = pleaseNote,
                        ticketLimit = ticketLimit,
                        address = address,
                        venue = venue,
                        city = city,
                        state = stateCode,
                        twitter = twitter
                    )
                    # event_city += 1
                    eventList.append(new_event) 
                    break #only need one
        except:
            print("nothing for city: " + city)
            print("this is the state code: " + stateCode)
            url = "https://app.ticketmaster.com/discovery/v2/events.json?stateCode={}&apikey={}".format(stateCode, api_keys['ticketmaster'])
            print("this is the url:  " + url)
            # sleep to ensure we don't make API requests too fast
            time.sleep(0.5)
            response = requests.get(url)
            data  = response.json()
            with open('events2.json', 'w') as convert_file:
                convert_file.write(json.dumps(data))
            try:
                for event in data["_embedded"]["events"]:
                    try:
                        id = event["id"]
                        if id in eventKeys:
                                print("go to the next event, this is a duplicate")
                                continue
                        else:
                            eventKeys.add(id)
                    except:
                        id = "NaN"
                    try:
                        name = event["name"]
                    except:
                        name = "NaN"
                    try: 
                        image = event["images"][0]["url"]
                    except:
                        image = "NaN"
                    try: 
                        startDate = event["dates"]["start"]["localDate"]
                    except:
                        startDate = "NaN"
                    try: 
                        startTime = event["dates"]["start"]["localTime"]
                    except:
                        startTime = "NaN"
                    try: 
                        status = event["dates"]["status"]["code"]
                    except:
                        status = "NaN"
                    try: 
                        segment = event["classifications"][0]["segment"]["name"]
                    except:
                        segment = "NaN"
                    try: 
                        genre = event["classifications"][0]["genre"]["name"]
                    except:
                        genre = "NaN"
                    try: 
                        promoter = event["promoter"]["name"]
                    except:
                        promoter = "NaN"
                    try: 
                        priceMin = event["priceRanges"][0]["min"]
                    except:
                        priceMin = -1 #"NaN"
                    try: 
                        priceMax = event["priceRanges"][0]["max"]
                    except:
                        priceMax = -1 #"NaN"
                    try: 
                        info = event["info"]
                    except:
                        info = "NaN"
                    try: 
                        pleaseNote = event["pleaseNote"]
                    except:
                        pleaseNote = "NaN"
                    try: 
                        ticketLimit = event["accessibility"]["ticketLimit"]
                    except:
                        ticketLimit = -1 #"NaN"
                    try: 
                        address = event["_embedded"]["venues"][0]["address"]["line1"]
                    except:
                        address = "NaN"
                    try: 
                        venue = event["_embedded"]["venues"][0]["name"]
                    except:
                        venue = "NaN"
                    try:
                        twitter = event["_embedded"]["attractions"][0]["externalLinks"]["twitter"][0]["url"]
                    except:
                        twitter = "NaN"
                
                    print("item:" + event["name"] + "\n")
                    new_event = Events(
                        id = id,
                        name = name,
                        image = image,
                        startDate = startDate,
                        startTime = startTime,
                        status = status,
                        segment = segment,
                        genre = genre,
                        promoter = promoter,
                        priceMin = priceMin,
                        priceMax = priceMax,
                        info = info,
                        pleaseNote = pleaseNote,
                        ticketLimit = ticketLimit,
                        address = address,
                        venue = venue,
                        city = city,
                        state = stateCode,
                        twitter = twitter
                    )
                    # event_state += 1
                    eventList.append(new_event) 
                    break #only need one
            except:
                print("something wrong !!!!!!!!!! WHY DON'T WE HAVE ANYTHING FOR THE STATE????!!!")
            print("-------------------------------------------------------------------")

    db.session.add_all(eventList)
    print("adding and committing changes");
    db.session.commit()


def __init__(
    self,
    id="NaN",
    name="NaN",
    address="NaN",
    image="NaN",
    startDate="NaN",
    startTime="NaN",
    status="NaN",
    segment="NaN",
    genre="NaN",
    promoter="NaN",
    info="NaN",
    pleaseNote="NaN",
    venue="NaN",
    priceMin = -1,
    priceMax = -1,
    ticketLimit=-1,
    city = "NaN",
    state = "NaN",
    twitter = "NaN"
):
    self.id = id
    self.name = name
    self.image = image
    self.startDate = startDate
    self.startTime = startTime
    self.status = status
    self.segment = segment
    self.genre = genre
    self.promoter = promoter
    self.priceMin = priceMin
    self.priceMax = priceMax
    self.info = info
    self.pleaseNote = pleaseNote
    self.ticketLimit = ticketLimit
    self.address = address
    self.venue = venue
    self.city = city
    self.state = state
    self.twitter = twitter

# function to retrieve data from CharityNavigator and store into database
def populate_charities():
    db.create_all()
    url = "https://api.data.charitynavigator.org/v2/Organizations"
    params = {"app_id": "60b6020c", "app_key" : api_keys['charitynavigator'], "rated" : "TRUE", "sort" : "RATING:DESC", "pageSize" : "200"}

    response = requests.get(url, params=params)
    data  = response.json()
    
    charityList = []
    cities = set()

    # https://charity.3scale.net/docs/data-api/reference#organization-collection
    
    
    for charity in data:
        charityEIN = charity["ein"]
        name = charity["charityName"]  # searchable
        address = charity["mailingAddress"]["streetAddress1"] # searchable
        mission = charity["mission"]    # searchable
        url = charity["websiteURL"]     # searchable 
        tagLine = charity["tagLine"] # searchable
        overallRating = charity["currentRating"]["score"] # sortable
        financialRating = charity["currentRating"]["financialRating"]["score"] # sortable
        city = charity["mailingAddress"]["city"].strip() # sortable
        state = charity["mailingAddress"]["stateOrProvince"].strip() # sortable
        zipCode = charity["mailingAddress"]["postalCode"] # sortable
        classification = charity["category"]["categoryID"] # sortable
        ratingImage = charity['currentRating']['ratingImage']['large']
        categoryImage = charity['category']['image']
        causeImage = charity['cause']['image']
    
        if city not in cities:
            cities.add(city)
            new_charity = Charities(
                charityEIN = charityEIN,
                name = name,
                address = address,
                mission = mission,
                url = url,
                tagLine = tagLine, 
                overallRating = overallRating,
                financialRating = financialRating,
                city = city,
                state = state,
                zipCode = zipCode,
                classification = classification,
                ratingImage = ratingImage,
                categoryImage = categoryImage,
                causeImage = causeImage
            )
            charityList.append(new_charity)

    db.session.add_all(charityList)
    db.session.commit()
   
def __init__(
    self,
    id=0,
    charityEIN=0,
    name="NaN",
    address="NaN",
    mission="NaN",
    url="NaN",
    tagLine="NaN",
    overallRating="NaN",
    financialRating="NaN",
    city="NaN",
    state="NaN",
    zipCode="NaN",
    classification="NaN",
    ratingImage = "NaN",
    categoryImage = "NaN",
    causeImage = "NaN"
    
):
    self.id = id
    self.charityEIN = charityEIN
    self.name = name
    self.address = address
    self.mission = mission
    self.url = url
    self.tagLine = tagLine
    self.overallRating = overallRating
    self.financialRating = financialRating
    self.city = city
    self.state = state
    self.zipCode = zipCode
    self.classification = classification
    self.ratingImage = ratingImage
    self.categoryImage = categoryImage
    self.causeImage = causeImage

# function to retrieve data from Roadgoat and store into database
def populate_locations():
    db.create_all()
    encoded_bytes = base64.b64encode(api_keys['roadgoat'].encode("utf-8"))
    auth_key = str(encoded_bytes, "utf-8")

    # authenticate API link
    headers = {
        'Connection': 'keep-alive',
        'Authorization': f'Basic {auth_key}'    
    }

    # Since Roadgoat does not provide a list of cities, use 
    # locations in Charities to get city instances
    x = db.session.query(Charities).all()
    cities = dict()
    for i in x :   
        city = i.city
        state = i.state
        city_split = city.split(".")
        if (len(city_split) > 1):
            new_city = city_split[1]
        new_city = city.strip().lower().replace(' ', '-')
        if city == 'Roanake' :
            new_city = 'roanoke'
        if city == 'Dulles' :
            new_city = 'dulles-town-center'
        if city == 'Clinton Twp' :
            new_city = 'clinton'
        if city == 'South Boston' :
            new_city = 'boston'
        if city == 'Overtown, Miami':
            new_city = 'miami'
        if city_split[0] == 'St' :
            if city == 'Petersburg' or city == 'Louis':
                new_city = 'st-' + city_split[1].strip().lower() + '-' + state.strip().lower() + '-usa'
            else:
                new_city = 'saint-' + city_split[1].strip().lower() + '-' + state.strip().lower() + '-usa'
        else :
            new_city = new_city + '-' + state.strip().lower() + '-usa' #s_to_a[state].strip().lower()
        cities[new_city] = city

    print(len(cities))

    url = "https://api.roadgoat.com/api/v2/destinations/"
    ct = 0

    # removes all files in ./roadgoat-json/
    files = glob.glob('./roadgoat-json/*')
    for f in files :
        os.remove(f)
    
    idxToName = dict()

    # Calls API and writes to files
    for i in cities:
        res = requests.get(url + i, headers=headers, verify=False)
        idxToName[ct] = cities[i]
        data = json.loads(res.text)
        with open('./roadgoat-json/location-' + str(ct) + '.json', 'w') as convert_file:
            convert_file.write(json.dumps(data))
        ct = ct + 1
    
    # Uses stored files to push to database

    locationList = []
    num_files = len(os.listdir("./roadgoat-json")) 
    for idx in range(len(cities)):
        file_name = 'roadgoat-json/location-' + str(idx) + '.json'
        f = open(file_name, "r")
        data = json.loads(f.read())
        name = ""
        try :
            name = data["data"]["attributes"]["name"] # searchable
        except KeyError:
            print("error in " + file_name)
            continue
        try :
            photo_id = data["data"]["relationships"]["photos"]["data"][0]["id"]
            included = data["included"]
            for j in range(len(included)) :
                if included[j]["id"] == photo_id :
                    photo_url = data["included"][j]["attributes"]["image"]["full"]
        except KeyError:
            photo_url = "null"
        name_split = name.split(",")
        state = "" + name_split[1].strip()
        population = data["data"]["attributes"]["population"] # sortable
        latitude = data["data"]["attributes"]["latitude"] # sortable
        longitude = data["data"]["attributes"]["longitude"] # sortable
        rating = data["data"]["attributes"]["average_rating"] # sortable
        budget = 0 # sortable/searchable. On a scale from 1-8 with 8 being most expensive
        budget_text = "" # searchable
        for location in data["data"]["attributes"]["budget"].items():
            curr_budget = data["data"]["attributes"]["budget"][location[0]]["value"]
            if location[0] != "United States" and curr_budget > budget :
                budget = curr_budget
                budget_text = data["data"]["attributes"]["budget"][location[0]]["text"]
        if budget == 0 :
            budget = data["data"]["attributes"]["budget"]["United States"]["value"]
            budget_text = data["data"]["attributes"]["budget"][location[0]]["text"]

        
        safety = 0 # sortable/searchable. On a scale from 1-5 with 5 being the safest
        safety_text = "" # searchable
        for location in data["data"]["attributes"]["safety"].items():
            curr_safety = data["data"]["attributes"]["safety"][location[0]]["value"]
            if location[0] != "United States" and curr_safety > safety :
                safety = curr_safety
                safety_text = data["data"]["attributes"]["safety"][location[0]]["text"]
        if safety == 0 :
            safety = data["data"]["attributes"]["safety"]["United States"]["value"]
            safety_text = data["data"]["attributes"]["safety"][location[0]]["text"]

        covid = 0 # sortable/searchable. On a scale from 1-5 with 5 being the safest
        covid_text = "" # searchable
        for location in data["data"]["attributes"]["covid"].items():
            curr_covid = data["data"]["attributes"]["covid"][location[0]]["value"]
            if location[0] != "United States" and curr_covid > covid :
                covid = curr_covid
                covid_text = data["data"]["attributes"]["covid"][location[0]]["text"]
        if covid == 0 :
            covid = data["data"]["attributes"]["covid"]["United States"]["value"]
            covid_text = data["data"]["attributes"]["covid"][location[0]]["text"]
        
        url = data['data']['attributes']['url']

        
        new_location = Locations(
            id = len(locationList),
            name = name, # searchable
            city = idxToName[idx], # searchable
            state = state, # searchable
            population = population, # sortable
            latitude = latitude, # sortable
            longitude = longitude, # sortable
            rating = rating, # sortable
            budget = budget, # sortable/searchable. On a scale from 1-8 with 8 being most expensive
            budget_text = budget_text, # searchable
            safety = safety, # sortable/searchable. On a scale from 1-5 with 5 being the safest
            safety_text = safety_text, # searchable
            covid =  covid, # sortable. New daily cases per 100k people
            covid_text = covid_text, # seachable
            photo_url = photo_url,
            url = url
        )
        print(name + ": ", idxToName[idx], state)

        locationList.append(new_location)
    db.session.add_all(locationList)
    db.session.commit()

def __init__(
    self,
    id=0,
    name="NaN",
    city="NaN",
    state="NaN",
    population=0,
    latitude=0,
    longitude=0,
    rating=0,
    budget=0,
    budget_text="NaN",
    safety=0,
    safety_text="NaN",
    covid=0,
    covid_text="NaN",
    photo_url="NaN",
    url="NaN"
    
):
    self.id = id
    self.name = name
    self.city = city
    self.state = state
    self.population = population
    self.latitude = latitude
    self.longitude = longitude
    self.rating = rating
    self.budget = budget
    self.budget_text = budget_text
    self.safety = safety
    self.safety_text = safety_text
    self.covid = covid
    self.covid_text = covid_text
    self.photo_url = photo_url
    self.url = url


# link all charities to corresponding event
def link_charities_events():
    charities = db.session.query(Charities).all()
    for charity in charities:
        event = db.session.query(Events).filter_by(city=charity.city, state=charity.state).first()
        charity.event_info = event
    db.session.commit()

# link all charities to corresponding location
def link_charities_locations():
    charities = db.session.query(Charities).all()
    for charity in charities:
        location = db.session.query(Locations).filter_by(city=charity.city, state=charity.state).first()
        charity.location_info = location
    db.session.commit()

# link all events to corresponding location
def link_events_locations():
    events = db.session.query(Events).all()
    for event in events:
        location = db.session.query(Locations).filter_by(city=event.city, state=event.state).first()
        event.location_info = location
    db.session.commit()

# function to delete the whole database
def reset_db():
    # Be VERYYY careful with this...
    db.session.remove()
    db.drop_all()
    db.create_all()
    print("Database reset")


if __name__ == "__main__":
    print("Initiating db push...")
    reset_db()
    populate_charities()
    populate_events()
    populate_locations()
    link_charities_events()
    link_charities_locations()
    link_events_locations()
