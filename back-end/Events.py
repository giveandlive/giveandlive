import sqlalchemy
from models import *
from sqlalchemy.sql.expression import null
from sqlalchemy import and_, cast, or_, func, tuple_
from query_helpers import *

# helper function to search events_query for q
def search_events(q, events_query):
    # if nothing to search by, do nothing
    if not q:
        return events_query
    else:
        q = q[0].strip()
    terms = q.split(' ')

    # define set of conditions for a term, where the filter 
    # has to have one of the criteria
    def searchTerm(term):
        searches = []
        # result's name or genre or segment, etc. must contain term
        searches.append(func.lower(Events.name).contains(func.lower(term)))
        searches.append(func.lower(Events.genre).contains(func.lower(term)))
        searches.append(func.lower(Events.segment).contains(func.lower(term)))
        searches.append(func.lower(Events.info).contains(func.lower(term)))
        searches.append(func.lower(Events.city).contains(func.lower(term)))
        searches.append(func.lower(Events.startDate).contains(func.lower(term)))
        searches.append(func.lower(Events.startTime).contains(func.lower(term)))
        searches.append(func.lower(Events.status).contains(func.lower(term)))
        return or_(*tuple(searches))

    # define filter needing to have all the terms in at least one of the fields
    events_query = events_query.filter(and_(*tuple((searchTerm(term) for term in terms))))
    return events_query

# helper function to sort all_events by request
def sort_events_by(sorting, all_events, desc):
    # switch based on requested field
    events = None

    if sorting == "name":
        events = Events.name
    elif sorting == "date":
        events = Events.startDate
    elif sorting == "time":
        events = Events.startTime
    elif sorting == "status":
        events = Events.status
    elif sorting == "genre":
        events = Events.genre
    elif sorting == "price":
        events = Events.priceMin
    else:
        return all_events
    
    # handle descending case, sort by given field
    if desc:
        return all_events.order_by(events.desc())
    else:
        return all_events.order_by(events)

# helper function to handle sorting all_events by query
def sort_events(sort, all_events):
    # if no query, do nothing
    if sort == None:
        return all_events
    else:
        sort = sort[0]
    sort = sort.split("-")
    
    # pass descending through to sort_events_by
    if sort[1] == "descending":
        return sort_events_by(sort[0], all_events, True)
    else:
        return sort_events_by(sort[0], all_events, False)

# helper function to handle filtering all_events by query
def filter_events(queries, all_events):
    # filter for events that are equal to requested value for their field
    status = get_query("status", queries)
    genre = get_query("genre", queries)
    if status != None :
        all_events = all_events.filter(func.lower(Events.status).in_(status))
    if genre != None :
        all_events = all_events.filter(func.lower(Events.genre).in_(genre))
    return all_events
