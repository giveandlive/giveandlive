from calendar import c
import os
from Charities import *
from Events import *
from Locations import *
from models import (
    Events,
    Charities,
    Locations,
    db,
    app,
    event_schema,
    charity_schema,
    location_schema,
)
from flask import Flask, request, make_response, jsonify, send_from_directory
from query_helpers import *
from Events import *
from Locations import *


# sitewide search endpoint
@app.route("/search", methods=["GET"])
def search():
    # retrieve API request args
    queries = request.args.to_dict(flat=False)
    
    # get requested page number, default page=1
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    # get max amount of objects for each model query, default per_page=10
    per_page = get_query("per_page", queries)
    if per_page == None:
        per_page = 10
    else:
        per_page = int(per_page[0])

    # start with queries of the whole tables
    charity_query = db.session.query(Charities)
    event_query = db.session.query(Events)
    locations_query = db.session.query(Locations)

    # narrow results via search query if provided
    data_query = get_query("search", queries)
    if data_query:
        charity_query = search_charities(data_query, charity_query)
        event_query = search_events(data_query, event_query)
        locations_query = search_locations(data_query, locations_query)

    # get the number of total results 
    charity_count = charity_query.count()
    event_count = event_query.count()
    locations_count = locations_query.count()
    
    # if page == -1, dump all results
    if page == -1:
        charity_result = charity_schema.dump(charity_query, many=True)
        event_result = event_schema.dump(event_query, many=True)
        location_result = location_schema.dump(locations_query, many=True)
    
    # if page != -1 paginate results and store
    else:
        try:
            all_charities = charity_query.paginate(page=page, per_page=per_page)
            charity_result = charity_schema.dump(all_charities.items, many=True)
        except:
            charity_result = []
        try:
            all_events = event_query.paginate(page=page, per_page=per_page)
            event_result = event_schema.dump(all_events.items, many=True)
        except:
            event_result = []
        try:
            all_locations = locations_query.paginate(page=page, per_page=per_page)
            location_result = location_schema.dump(all_locations.items, many=True)
        except:
            location_result = []

    return {
        "charity_count": charity_count,
        "charity": charity_result,
        "event_count": event_count,
        "event": event_result,
        "location_count": locations_count,
        "location": location_result,
    }


# ---------- Events ----------

# event model endpoint
@app.route("/event", methods=["GET"])
def events():

    # retrieve API request args, get only the needed cols
    queries = request.args.to_dict(flat=False)
    event_query = db.session.query(Events.id, Events.name, Events.startDate, Events.startTime, Events.status,
                                   Events.genre, Events.priceMin)
    
    # get max amount of objects for each model query, default per_page=10
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])
        
    # searching
    q = get_query("search", queries)
    query_length = event_query.count()
    if q:
        event_query = search_events(q, event_query)

    # sorting
    sort = get_query("sort", queries)
    event_query = sort_events(sort, event_query)

    # filtering
    statuses = get_query("statuses", queries)
    if statuses:
        page = -1
        event_query = event_query.with_entities(Events.status)
    genres = get_query("genres", queries)
    if genres:
        page = -1
        event_query = event_query.with_entities(Events.genre)

    event_query = filter_events(queries, event_query)
    query_length = event_query.count()
    
    # retrieve results from PostGres, paginated or not
    if page == -1:
        content = event_schema.dump(event_query, many=True)
    else:
        all_pages = event_query.paginate(page=page, per_page=20)
        content = event_schema.dump(all_pages.items, many=True)

    return {"content": content, "num": query_length}

# endpoint for event instance
@app.route("/event/<string:id>", methods=["GET"])
def event_id(id):
    # retrieve event that fits the given ID, return to user
    content = db.session.query(Events).filter_by(id=id)
    event = event_schema.dump(content, many=True)[0]
    return event


# ---------- Charities ----------

# charity model endpoint
@app.route("/charity", methods=["GET"])
def charities():

    # retrieve API request args, get only the needed cols
    queries = request.args.to_dict(flat=False)
    charity_query = db.session.query(Charities.id, Charities.name, Charities.overallRating, Charities.city,
                                    Charities.state, Charities.zipCode, Charities.classification)
    
    # get max amount of objects for each model query, default per_page=10
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])

    # searching
    q = get_query("search", queries)
    if q:
        charity_query = search_charities(q, charity_query)
    query_length = charity_query.count()

    # sorting
    sort = get_query("sort", queries)
    charity_query = sort_charities(sort, charity_query)

    # filtering
    classifications = get_query("classifications", queries)
    if (classifications and len(classifications)>0 and classifications[0] == ''):
        page = -1
        charity_query = charity_query.with_entities(Charities.classification)
    ratings = get_query("ratings", queries)
    if ratings:
        page = -1
        charity_query = charity_query.with_entities(Charities.overallRating)

    charity_query = filter_charities(queries, charity_query)
    query_length = charity_query.count()

    # retrieve results from PostGres, paginated or not
    if page == -1:
        content = charity_schema.dump(charity_query, many=True)
    else:
        all_pages = charity_query.paginate(page=page, per_page=20)
        content = charity_schema.dump(all_pages.items, many=True)

    return {"content": content, "num": query_length}

# endpoint for charity instance
@app.route("/charity/<int:id>", methods=["GET"])
def charity_id(id):
    # retrieve event that fits the given ID, return to user
    content = db.session.query(Charities).filter_by(id=id)
    charity = charity_schema.dump(content, many=True)[0]
    return charity


# ---------- Locations ----------

# charity model endpoint
@app.route("/location", methods=["GET"])
def locations():

    # retrieve API request args, get only the needed cols
    queries = request.args.to_dict(flat=False)
    location_query = db.session.query(Locations.id, Locations.name, Locations.latitude, Locations.longitude,
                                        Locations.budget, Locations.population, Locations.safety)
    
    # get max amount of objects for each model query, default per_page=10
    page = get_query("page", queries)
    if page == None:
        page = 1
    else:
        page = int(page[0])
        
    # searching
    q = get_query("search", queries)
    if q:
        location_query = search_locations(q, location_query)

    # sorting
    sort = get_query("sort", queries)
    location_query = sort_locations(sort, location_query)

    # filtering
    budgets = get_query("budgets", queries)
    if budgets:
        page = -1
        location_query = location_query.with_entities(Locations.budget)
    safeties = get_query("safeties", queries)
    if safeties:
        page = -1
        location_query = location_query.with_entities(Locations.safety)
    
    location_query = filter_locations(queries, location_query)
    query_length = location_query.count()

    # retrieve results from PostGres, paginated or not
    if page == -1:
        content = location_schema.dump(location_query, many=True)
    else:
        all_pages = location_query.paginate(page=page, per_page=20)
        content = location_schema.dump(all_pages.items, many=True)

    return {"content": content, "num": query_length}

# endpoint for location instance
@app.route("/location/<int:id>", methods=["GET"])
def location_id(id):
    content = db.session.query(Locations).filter_by(id=id)
    location = location_schema.dump(content, many=True)[0]
    return location

# default API link behavior
@app.route("/")
def api_home():
    return "<h1>Welcome to Give & Live's RESTful API!</h1>"

# favicon for API
@app.route("/favicon.ico")
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, "static"),
        "icon.ico",
        mimetype="image/vnd.microsoft.icon",
    )

if __name__ == "__main__":
    app.run()
