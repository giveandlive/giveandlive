from dataclasses import field
from flask import Flask, request, make_response, jsonify
from db import init_db
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from marshmallow import fields, post_dump

app = Flask(__name__)
app.config['DEBUG'] = True
app.debug = True
db = init_db(app)
ma = Marshmallow(app)
cors = CORS(app)

# events table object with specified column names
class Events(db.Model):
    __tablename__ = "events"
    id = db.Column(db.String(), primary_key=True)
    name = db.Column(db.String())
    image = db.Column(db.String())
    startDate = db.Column(db.String())
    startTime = db.Column(db.String())
    status = db.Column(db.String())
    segment = db.Column(db.String())
    genre = db.Column(db.String())
    promoter = db.Column(db.String())
    priceMin = db.Column(db.Integer)
    priceMax = db.Column(db.Integer)
    info = db.Column(db.String())
    pleaseNote = db.Column(db.String())
    ticketLimit = db.Column(db.Integer)
    address = db.Column(db.String())
    venue = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    twitter = db.Column(db.String())
    charity_info = db.relationship("Charities", backref="event_info")
    location = db.Column(db.Integer, db.ForeignKey("locations.id"), nullable=True)

# charities table object with specified column names
class Charities(db.Model):
    __tablename__ = "charities"
    id = db.Column(db.Integer, primary_key=True)
    charityEIN = db.Column(db.Integer)
    name = db.Column(db.String())
    address = db.Column(db.String())
    mission = db.Column(db.String())
    url = db.Column(db.String())
    tagLine = db.Column(db.String())
    overallRating = db.Column(db.String())
    financialRating = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    zipCode = db.Column(db.String())
    classification = db.Column(db.String())
    ratingImage = db.Column(db.String())
    categoryImage = db.Column(db.String())
    causeImage = db.Column(db.String())
    event = db.Column(db.String, db.ForeignKey("events.id"), nullable=True)
    location = db.Column(db.Integer, db.ForeignKey("locations.id"), nullable=True)

# locations table object with specified column names
class Locations(db.Model):
    __tablename__ = "locations"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    population = db.Column(db.Integer)
    latitude = db.Column(db.Integer)
    longitude = db.Column(db.Integer)
    rating = db.Column(db.Integer)
    budget = db.Column(db.Integer)
    budget_text = db.Column(db.String())
    safety = db.Column(db.String())
    safety_text = db.Column(db.String())
    covid = db.Column(db.Integer)
    covid_text = db.Column(db.String())
    photo_url = db.Column(db.String())
    url = db.Column(db.String())
    charity_info = db.relationship("Charities", backref="location_info")
    event_info = db.relationship("Events", backref="location_info")


# initial schema formatting class
class BaseSchema(ma.Schema):
    SKIP_VALUES = [None]

    @post_dump
    def remove_skip_values(self, data, **kargs):
        return {
            key: value for key, value in data.items() if value not in self.SKIP_VALUES
        }

# events json schema for API call results
class EventsSchema(BaseSchema):
    id = fields.Str(required=True)
    name = fields.Str(required=True)
    image = fields.Str(required=True)
    startDate = fields.Str(required=True)
    startTime = fields.Str(required=True)
    status = fields.Str(required=True)
    segment = fields.Str(required=True)
    genre = fields.Str(required=True)
    promoter = fields.Str(required=True)
    priceMin = fields.Int(required=True)
    priceMax = fields.Int(required=True)
    info = fields.Str(required=True)
    pleaseNote = fields.Str(required=True)
    ticketLimit = fields.Int(required=True)
    address = fields.Str(required=True)
    venue = fields.Str(required=True)
    city = fields.Str(required=True)
    state = fields.Str(required=True)
    twitter = fields.Str(required=True)
    charity_info = fields.Nested("CharitiesSchema", only=("id", "name"), required=True, many=True)
    location_info = fields.Nested("LocationsSchema", only=("id", "name"), required=True)

# charities json schema for API call results
class CharitiesSchema(BaseSchema):
    id = fields.Int(required=True)
    charityEIN = fields.Int(required=True)
    name = fields.Str(required=True)
    address = fields.Str(required=True)
    mission = fields.Str(required=True)
    url = fields.Str(required=True)
    tagLine = fields.Str(required=True)
    overallRating = fields.Float(required=True)
    financialRating = fields.Str(required=True)
    city = fields.Str(required=True)
    state = fields.Str(required=True)
    zipCode = fields.Str(required=True)
    classification = fields.Str(required=True)
    ratingImage = fields.Str(required=True)
    categoryImage = fields.Str(required=True)
    causeImage = fields.Str(required=True)
    event_info = fields.Nested("EventsSchema", only=("id", "name"), required=True)
    location_info = fields.Nested("LocationsSchema", only=("id", "name"), required=True)

# locations json schema for API call results
class LocationsSchema(BaseSchema):
    id = fields.Int(required=True)
    name = fields.Str(required=True)
    city = fields.Str(required=True)
    state = fields.Str(required=True)
    population = fields.Int(required=True)
    latitude = fields.Float(required=True)
    longitude = fields.Float(required=True)
    rating = fields.Int(required=True)
    budget = fields.Int(required=True)
    budget_text = fields.Str(required=True)
    safety = fields.Int(required=True)
    safety_text = fields.Str(required=True)
    covid = fields.Int(required=True)
    covid_text = fields.Str(required=True)
    photo_url = fields.Str(required=True)
    url = fields.Str(required=True)
    charity_info = fields.Nested("CharitiesSchema", only=("id", "name"), required=True, many=True)
    event_info = fields.Nested("EventsSchema", only=("id", "name"), required=True, many=True)

event_schema = EventsSchema()
charity_schema = CharitiesSchema()
location_schema = LocationsSchema()
