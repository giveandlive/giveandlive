from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from sqlalchemy import create_engine, Column, String, Integer
import os

# link PostGres database using URL link
def init_db(app):
    load_dotenv()
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("AWS_DB_KEY") 
    return SQLAlchemy(app)

