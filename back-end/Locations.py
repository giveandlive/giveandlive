import sqlalchemy
from models import *
from sqlalchemy.sql.expression import null
from sqlalchemy import and_, cast, or_, func, tuple_
from query_helpers import *

# helper function to search locations_query for q
def search_locations(q, locations_query):
    # if nothing to search by, do nothing
    if not q:
        return locations_query
    else:
        q = q[0].strip()
    terms = q.split(' ')

    # define set of conditions for a term, where the filter 
    # has to have one of the criteria
    def searchTerm(term):
        searches = []
        # result's name or safety_text or covid_text, etc.  must contain term
        searches.append(func.lower(Locations.name).contains(func.lower(term)))
        searches.append(func.lower(Locations.safety_text).contains(func.lower(term)))
        searches.append(func.lower(Locations.covid_text).contains(func.lower(term)))
        searches.append(func.lower(Locations.budget_text).contains(func.lower(term)))
        searches.append(func.lower(cast(Locations.latitude, sqlalchemy.String)).contains(func.lower(term)))
        searches.append(func.lower(cast(Locations.longitude, sqlalchemy.String)).contains(func.lower(term)))
        searches.append(func.lower(cast(Locations.population, sqlalchemy.String)).contains(func.lower(term)))
        searches.append(func.lower(cast(Locations.budget, sqlalchemy.String)).contains(func.lower(term)))
        searches.append(func.lower(Locations.safety).contains(func.lower(term)))
        return or_(*tuple(searches))
        
    # define filter needing to have all the terms in at least one of the fields
    locations_query = locations_query.filter(and_(*tuple((searchTerm(term) for term in terms))))
    return locations_query

# helper function to sort all_locations by request
def sort_locations_by(sorting, all_locations, desc):
    # switch based on requested field
    locations = None

    if sorting == "name":
        locations = Locations.name
    elif sorting == "latitude":
        locations = Locations.latitude
    elif sorting == "longitude":
        locations = Locations.longitude
    elif sorting == "budget":
        locations = Locations.budget
    elif sorting == "population":
        locations = Locations.population
    elif sorting == "safety":
        locations = Locations.safety
    else:
        return all_locations
    
    # handle descending case, sort by given field 
    if desc:
        return all_locations.order_by(locations.desc())
    else:
        return all_locations.order_by(locations)

# helper function to handle sorting all_locations by query
def sort_locations(sort, all_locations):
    # if no query, do nothing
    if sort == None:
        return all_locations
    else:
        sort = sort[0]
    sort = sort.split("-")
    
    # pass descending through to sort_locations_by
    if sort[1] == "descending":
        return sort_locations_by(sort[0], all_locations, True)
    else:
        return sort_locations_by(sort[0], all_locations, False)

# helper function to handle filtering all_locations by query
def filter_locations(queries, all_locations):
    # filter for charities that are equal to requested value for their field
    budget = get_query("budget", queries)
    safety = get_query("safety", queries)
    if budget != None :
        all_locations = all_locations.filter(Locations.budget.in_(budget))
    if safety != None :
        all_locations = all_locations.filter(func.lower(Locations.safety).in_(safety))
    return all_locations
