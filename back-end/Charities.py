import sqlalchemy
from models import *
from sqlalchemy.sql.expression import null
from sqlalchemy import and_, cast, or_, func, tuple_
from query_helpers import *

# helper function to search charities_query for q
def search_charities(q, charities_query):
    # if nothing to search by, do nothing
    if not q:
        return charities_query
    else:
        q = q[0].strip()
    terms = q.split(' ')
    
    # define set of conditions for a term, where the filter 
    # has to have one of the criteria
    def searchTerm(term):
        searches = []
        # result's name or city or state, etc.  must contain term
        searches.append(func.lower(Charities.name).contains(func.lower(term)))
        searches.append(func.lower(Charities.city).contains(func.lower(term)))
        searches.append(func.lower(Charities.state).contains(func.lower(term)))
        searches.append(func.lower(Charities.tagLine).contains(func.lower(term)))
        searches.append(func.lower(Charities.overallRating).contains(func.lower(term)))
        searches.append(func.lower(Charities.zipCode).contains(func.lower(term)))
        searches.append(func.lower(Charities.classification).contains(func.lower(term)))
        return or_(*tuple(searches))

    # define filter needing to have all the terms in at least one of the fields
    charities_query = charities_query.filter(and_(*list((searchTerm(term) for term in terms))))
    return charities_query

# helper function to sort all_charities by request
def sort_charities_by(sorting, all_charities, desc):
    # switch based on requested field, datatype
    charities = None
    isInteger = False

    if sorting == "name":
        charities = Charities.name
    elif sorting == "rating":
        isInteger = True
        charities = Charities.overallRating
    elif sorting == "city":
        charities = Charities.city
    elif sorting == "state":
        charities = Charities.state
    elif sorting == "zipcode":
        isInteger = True
        charities = Charities.zipCode
    elif sorting == "classification":
        isInteger = True
        charities = Charities.classification
    else:
        return all_charities
    
    # handle descending case, sort by given field (cast as int if needed)
    if desc:
        if isInteger :
            return all_charities.order_by(cast(charities, sqlalchemy.Float).desc())
        else :
            return all_charities.order_by(charities.desc())
    else:
        if isInteger :
            return all_charities.order_by(cast(charities,sqlalchemy.Float))
        else :
            return all_charities.order_by(charities)

# helper function to handle sorting all_charities by query
def sort_charities(sort, all_charities):
    # if no query, do nothing
    if sort == None:
        return all_charities
    else:
        sort = sort[0]
    sort = sort.split("-")
    
    # pass descending through to sort_charities_by
    if sort[1] == "descending":
        return sort_charities_by(sort[0], all_charities, True)
    else:
        return sort_charities_by(sort[0], all_charities, False)

# helper function to handle filtering all_charities by query
def filter_charities(queries, all_charities):
    # filter for charities that are equal to requested value for their field
    classification = get_query("classification", queries)
    rating = get_query("rating", queries)
    if classification != None :
        all_charities = all_charities.filter(func.lower(Charities.classification).in_(classification))
    if rating != None :
        all_charities = all_charities.filter(func.lower(Charities.overallRating).in_(rating))
    return all_charities
