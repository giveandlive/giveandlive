from cgi import test
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import time
from selenium.webdriver.common.keys import Keys


ser = Service("./front-end/selenium/chromedriver")
op = webdriver.ChromeOptions()
op.add_argument("--no-sandbox")
op.add_argument("--window-size=1420,1080")
op.add_argument("--headless")
op.add_argument("--disable-gpu")

driver = webdriver.Chrome(service=ser, options=op)
URL = "https://www.giveandlive.me"
driver.get(URL)

def test_navbar_home():
    print("starting test_navbar_home")
    home = driver.find_element(By.LINK_TEXT, "Give & Live")
    home.click()
    assert driver.current_url == URL + "/home/"

def test_navbar_charities():
    print("starting test_navbar_charities")
    charities = driver.find_element(By.LINK_TEXT, "Charities")
    charities.click()
    assert driver.current_url == URL + "/charities/"

def test_navbar_events():
    print("starting test_navbar_events")
    events = driver.find_element(By.LINK_TEXT, "Events")
    events.click()
    assert driver.current_url == URL + "/events/"

def test_navbar_locations():
    print("starting test_navbar_locations")
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == URL + "/locations/"

def test_navbar_about():
    print("starting test_navbar_about")
    about = driver.find_element(By.LINK_TEXT, "About")
    about.click()
    assert driver.current_url == URL + "/about/"

def test_navbar_about_back():
    print("starting test_navbar_about_back")
    driver.get(URL)
    about = driver.find_element(By.LINK_TEXT, "About")
    about.click()
    assert driver.current_url == URL + "/about/"
    driver.back()
    assert driver.current_url == URL + "/"

def test_navbar_charities_back():
    print("starting test_navbar_charities_back")
    driver.get(URL)
    centers = driver.find_element(By.LINK_TEXT, "Charities")
    centers.click()
    assert driver.current_url == URL + "/charities/"
    driver.back()
    assert driver.current_url == URL + "/"

def test_navbar_events_back():
    print("starting test_navbar_events_back")
    driver.get(URL)
    species = driver.find_element(By.LINK_TEXT, "Events")
    species.click()
    assert driver.current_url == URL + "/events/"
    driver.back()
    assert driver.current_url == URL + "/"

def test_navbar_locations_back():
    print("starting test_navbar_locations_back")
    driver.get(URL)
    species = driver.find_element(By.LINK_TEXT, "Locations")
    species.click()
    assert driver.current_url == URL + "/locations/"
    driver.back()
    assert driver.current_url == URL + "/"

def test_charities_instance():
    print("starting test_charities_instance")
    driver.get(URL)
    charities = driver.find_element(By.LINK_TEXT, "Charities")
    charities.click()
    assert driver.current_url == URL + "/charities/"
    driver.implicitly_wait(20)
    instance = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    instance.click()
    assert driver.current_url == URL + "/charities/30/"

def test_events_instance():
    print("starting test_events_instance")
    driver.get(URL)
    events = driver.find_element(By.LINK_TEXT, "Events")
    events.click()
    assert driver.current_url == URL + "/events/"
    driver.implicitly_wait(20)
    instance = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    instance.click()
    assert driver.current_url == URL + "/events/Z7r9jZ1AdF0FI/"
    
def test_locations_instance():
    print("starting test_locations_instance")
    driver.get(URL)
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == URL + "/locations/"
    driver.implicitly_wait(20)
    instance = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    instance.click()
    assert driver.current_url == URL + "/locations/0/"

def test_charities_sort():
    print("starting test_charities_sort")
    driver.get(URL)
    charities = driver.find_element(By.LINK_TEXT, "Charities")
    charities.click()
    assert driver.current_url == URL + "/charities/"
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div")
    # select by visible text
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div/div/div[2]/div") 
    select.click()
    driver.implicitly_wait(70)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div/div[2]/div/div[2]")
    select.click()
    # driver.implicitly_wait(100)
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/charities/141/"
    
def test_events_sort():
    print("starting test_events_sort")
    driver.get(URL)
    events = driver.find_element(By.LINK_TEXT, "Events")
    events.click()
    assert driver.current_url == URL + "/events/"
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div")
    # select by visible text
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div/div/div[2]/div") 
    select.click()
    driver.implicitly_wait(70)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div/div[2]/div/div[2]")
    select.click()
    # driver.implicitly_wait(100)
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/events/rZ7HnEZ1AoOZa4/"
    
def test_locations_sort():
    print("starting test_locations_sort")
    driver.get(URL)
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == URL + "/locations/"
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div")
    # select by visible text
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div/div/div[2]/div") 
    select.click()
    driver.implicitly_wait(70)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[3]/div/div[2]/div/div[2]")
    select.click()
    # driver.implicitly_wait(100)
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/locations/56/"
    
def test_charities_filter():
    print("starting test_charities_filter")
    driver.get(URL)
    charities = driver.find_element(By.LINK_TEXT, "Charities")
    charities.click()
    assert driver.current_url == URL + "/charities/"
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div")
    # select by visible text
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div/div/div[2]/div") 
    select.click()
    driver.implicitly_wait(70)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div/div[2]/div/div[2]")
    select.click()
    # driver.implicitly_wait(100)
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/charities/34/"
    
def test_events_filter():
    print("starting test_events_filter")
    driver.get(URL)
    events = driver.find_element(By.LINK_TEXT, "Events")
    events.click()
    assert driver.current_url == URL + "/events/"
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div")
    # select by visible text
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div/div/div[2]/div") 
    select.click()
    driver.implicitly_wait(70)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div/div[2]/div/div[2]")
    select.click()
    # driver.implicitly_wait(100)
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/events/Z7r9jZ1AdF0FI/"

def test_locations_filter():
    print("starting test_locations_filter")
    driver.get(URL)
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == URL + "/locations/"
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div")
    # select by visible text
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div/div/div[2]/div") 
    select.click()
    driver.implicitly_wait(70)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[1]/div/div[2]/div/div[2]")
    select.click()
    # driver.implicitly_wait(100)
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/locations/9/"

def test_charities_search():
    print("starting test_charities_search")
    driver.get(URL)
    events = driver.find_element(By.LINK_TEXT, "Charities")
    events.click()
    assert driver.current_url == URL + "/charities/"
    driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[4]/input").send_keys("houston");
    button = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[4]/button")
    button.click()
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/charities/23/"
    
def test_events_search():
    print("starting test_events_search")
    driver.get(URL)
    events = driver.find_element(By.LINK_TEXT, "Events")
    events.click()
    assert driver.current_url == URL + "/events/"
    driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[4]/input").send_keys("houston");
    button = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[4]/button")
    button.click()
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/events/Z7r9jZ1AduKA3/"
    
def test_locations_search():
    print("starting test_locations_search")
    driver.get(URL)
    locations = driver.find_element(By.LINK_TEXT, "Locations")
    locations.click()
    assert driver.current_url == URL + "/locations/"
    driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[4]/input").send_keys("houston");
    button = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/div[4]/button")
    button.click()
    time.sleep(4)
    select = driver.find_element(By.XPATH, "/html/body/div/div/div/div/table/tbody/tr[1]")
    select.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/locations/23/"
    
def test_search():
    print("starting test_search")
    driver.get(URL)
    search = driver.find_element(By.LINK_TEXT, "Search")
    search.click()
    assert driver.current_url == URL + "/search/"
    button = driver.find_element(By.XPATH, "/html/body/div/div/div/div/center/form/div/input")
    button.send_keys("houston")
    button.send_keys(Keys.ENTER)
    time.sleep(4)
    row = driver.find_element(By.XPATH, "/html/body/div/div/div/div/div[1]/center[1]/table/tbody/tr[1]")
    row.click()
    driver.implicitly_wait(70)
    assert driver.current_url == URL + "/charities/23/"

def selenium_test():
    test_navbar_home()
    test_navbar_charities()
    test_navbar_events()
    test_navbar_locations()
    test_navbar_about()
    test_navbar_about_back()
    test_navbar_charities_back()
    test_navbar_events_back()
    test_navbar_locations_back()
    test_charities_instance()
    test_events_instance()
    test_locations_instance()
    test_charities_sort()
    test_events_sort()
    test_locations_sort()
    test_charities_filter()
    test_events_filter()
    test_locations_filter()
    test_charities_search()
    test_events_search()
    test_locations_search()
    test_search()

selenium_test()