import React from "react"
import { Space, Typography } from "antd"
import styles from "./Visualizations.module.css"
import GenrePieChart from "./Visualizations/GenrePieChart"
import PopulationChart from "./Visualizations/PopulationChart"
import BillsPieChart from "./Visualizations/BillsPieChart"
import CharitiesChart from "./Visualizations/CharitiesMap.tsx"
import CPBarChart from "./Visualizations/CongressPeople"
import CharityChart from "./Visualizations/ClassificationChart"
import StateChart from "./Visualizations/State"
const { Title } = Typography

const Visualizations = () => {
	return (
		<main className={styles.wrapper}>
			<Title level={1} className={styles.header}>
				Visualizations
			</Title>
			<Title level={2}>Our Visualizations</Title>
			<h3 className={styles.centerText}>Events by Genre</h3>
			<GenrePieChart />  
			<h3 className={styles.centerText}>Locations per Population Range</h3>
			<PopulationChart />
			<h3 className={styles.centerText}>Charities per Charity Classification</h3>
			<CharityChart />
			{/* <CharitiesChart /> */}
			<Title level={2}>Provider Visualizations</Title>
			<h3 className={styles.centerText}>Bills by the Proposing Committee</h3>
			<BillsPieChart />
			<h3 className={styles.centerText}>Congresspeople by Party and Gender</h3>
			<CPBarChart />
			<h3 className={styles.centerText}>Number of Bills and Population per State</h3>
			<StateChart/>
		</main>
	)
}

export default Visualizations