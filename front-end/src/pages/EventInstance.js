import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import { Card, Button, Nav, Table } from 'react-bootstrap';
import axios from 'axios'
import './EventInstance.css';

// Event Instance element
const EventInstance = () =>  {

    const params = useParams();  /* stores the eventID */

    const [events_query, setEvents_query] = useState({}); /* Hook for API result */
    const [gettingData, setGettingData] = useState(false); /* Hook for when waiting for API call to return */
    const [location, setLocation] = useState(""); /* Hook to keep track of Event location for Google Maps */
    const [link_charity, setLink_charity] = useState({'id':0, 'name': 'none'}); /* Hook for link to CharityInstance */
    const [link_location, setLink_location] = useState({'id':0, 'name': 'none'}); /* Hook for link to LocationInstance */

    const api_key = process.env.REACT_APP_API_KEY; /* API Key for Google Maps */

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the requested event instance from useParams()
     */
    const get_data = async() => {

        setGettingData(true);

        // call api.giveandlive.me for eventInstance info
        const url = 'https://api.giveandlive.me/event/' + params.eventId; 
        const c_q = await axios.get(url);
        
        const linkCharity  = (c_q.data['charity_info' ].length>0) ? c_q.data['charity_info'][0] : undefined;
        const linkLocation = (c_q.data['location_info']=={}) ? undefined : c_q.data['location_info'];
        setLink_charity(linkCharity)
        setLink_location(linkLocation)
        
        // use API key and event instance location data to construct 
        // API call to Google Maps API
        const mapurl = "https://www.google.com/maps/embed/v1/place?key=" + api_key + "&q="+
            String(c_q.data['address']).trim().replaceAll(" ", "+").replace("&", "") + 
            ((linkLocation == undefined) ? "" : ","+linkLocation['name'].trim().replaceAll(" ", "+"));
        setLocation(mapurl);
    
        // save links for later
        setEvents_query(c_q.data);
        setGettingData(false);
    }

    // function called on page load to retrieve data initially
    useEffect(() => {
        get_data();
    }, [])

    // HTML output
    return (
        <center className='mainCenter'>
        {/* If we're loading data, don't render anything */}
        {!gettingData &&
            <div className='templatePage'>
                <center>
                {/* Main Card on left that gives instance information */}
                <Card className='attributeCard'>
                    <Card.Title style={{ fontSize: '6vh' }}>{events_query['name']}</Card.Title>
                    <center>
                    <Card.Body style={{width: "450px"}}>
                        <div>
                            <Table striped bordered hover>
                                <thead>
                                    <tr>
                                        <th>Attribute</th>
                                        <th>Value</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>Date</td>
                                        <td>{events_query['startDate']}</td>
                                    </tr>
                                    <tr>
                                        <td>Time</td>
                                        <td>{events_query['startTime']}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>{events_query['status']}</td>
                                    </tr>
                                    <tr>
                                        <td>Segment</td>
                                        <td>{events_query['segment']}</td>
                                    </tr>
                                    <tr>
                                        <td>Genre</td>
                                        <td>{events_query['genre']}</td>
                                    </tr>
                                    <tr>
                                        <td>Promoter</td>
                                        <td>{events_query['promoter'] !== 'NaN' && events_query['promoter']}
                                            {events_query['promoter']  == 'NaN' && "No Data"}
                                    </td>
                                    </tr>
                                    <tr>
                                        <td>Minimum Price</td>
                                        <td>{events_query['priceMin'] != -1 && events_query['priceMin']}
                                            {events_query['priceMin'] == -1 && "No Data"}</td>
                                    </tr>
                                    <tr>
                                        <td>Maximum Price</td>
                                        <td>{events_query['priceMax'] != -1 && events_query['priceMax']}
                                            {events_query['priceMax'] == -1 && "No Data"}</td>
                                    </tr>
                                    <tr>
                                        <td>Info</td>
                                        <td>{events_query['info'] !== 'NaN' && events_query['info']}
                                            {events_query['info']  == 'NaN' && "No Data"}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>{events_query['address']}</td>
                                    </tr>
                                    <tr>
                                        <td>Social Media Link</td>
                                        { events_query['twitter'] !== 'NaN' &&
                                        <td><a href={events_query['twitter']}>{events_query['twitter']}</a></td>}
                                        { events_query['twitter'] == 'NaN' &&
                                        <td>No Data</td>}
                                    </tr>
                                </tbody>
                            </Table>
                        </div>
                    </Card.Body>
                    </center>
                </Card>

                {/* Table on right side with map, images, and instance links */}
                <div style={{width: '650px', padding: '0px', float: 'left'}}>
                    <Table className='mediaTable'>
                        <tbody>
                            {/* Google Maps Embed and Images */}
                            <tr>
                                {/* Google Maps Embed */}
                                <td><center><iframe
                                    className='imgEmbed'
                                    loading="lazy"
                                    allowFullScreen
                                    referrerPolicy="no-referrer-when-downgrade"
                                    src={location}>
                                    </iframe></center></td>

                                {/* Images */}
                                <td>
                                    <center>
                                    <img src={events_query['image']} className='imgEmbed'/>
                                    </center>
                                </td>
                            </tr>

                            {/* Bottom row with links to other models */}
                            <tr>
                                {/* Charity Link */}
                                <td>
                                    <center>
                                    <p>Nearby Charity:</p>
                                    <Nav.Link 
                                    onClick={() => window.location.href="/charities/"+link_charity['id']}>
                                            {link_charity != undefined && 
                                            <Button variant="primary" className='linkButton'> {link_charity['name']} </Button>
                                            }
                                            {link_charity == undefined && "None"}
                                    </Nav.Link>
                                    </center>
                                </td>
                                {/* Location Link */}
                                <td>
                                    <center>
                                        <p>Nearby Location:</p>
                                        <Nav.Link onClick={() => window.location.href="/locations/"+link_location['id']}>
                                            {link_location != undefined && 
                                            <Button variant="primary" className='linkButton'>
                                                {link_location['name']}
                                            </Button>}
                                            
                                            {link_location == undefined && "None"}
                                        </Nav.Link>
                                    </center>
                                </td>
                            </tr>
                        </tbody>
                        
                    </Table>
                </div>
                </center>
            </div>
        }
        </center>
    );

}

export default EventInstance;