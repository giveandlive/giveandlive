import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import { Card, Button, Nav, Table } from 'react-bootstrap';
import axios from 'axios';

import './LocationInstance.css';

// Location Instance Element
const LocationInstance = () =>  {

    let params = useParams(); /* stores the eventID */

    const [locations_query, setLocations_query] = useState({}); /* Hook for API result */
    const [gettingData, setGettingData] = useState(false); /* Hook for when waiting for API call to return */
    const [gotGeolocationData, setGotGeolocationData] = useState(false); /* Hook for whether or not geolocation succeeded */
    const [geolocation, setGeolocation] = useState({'latitude': 0, 'longitude': 0}); /* Hook for user loat long */
    const [location, setLocation] = useState(""); /* Hook to keep track of Event location for Google Maps */
    const [link_charity, setLink_charity] = useState({'id':0, 'name': 'none'}); /* Hook for link to CharityInstance */
    const [link_event, setLink_event] = useState({'id':0, 'name': 'none'}); /* Hook for link to EventInstance */

    const api_key = process.env.REACT_APP_API_KEY; /* API Key for Google Maps */

    /* 
     * function called on successful geolocation query to store 
     * lat long information
     */
    function success(position) {

        // const lat  = position['coords']['latitude'];
        // const long = position['coords']['longitude'];
    
        setGeolocation(position['coords']);
        setGotGeolocationData(true);
    }

    /* 
     * function called on failed goelocation query
     */
    function failure() {
        console.log("failed to get use location");
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the requested location instance from useParams()
     */
    const get_data = async() => {
        setGettingData(true);

        // call api.giveandlive.me for locationInstance info
        const url = 'https://api.giveandlive.me/location/' + params.locationId; 
        const c_q = await axios.get(url);

        // use API key and charity instance location data to construct 
        // API call to Google Maps API
        const mapurl = "https://www.google.com/maps/embed/v1/place?key=" + api_key + "&q="+
        String(c_q.data['name'])
        setLocation(mapurl);
        setLocations_query(c_q.data);

        // call geolocation API
        let x = navigator.geolocation;
        x.getCurrentPosition(success, failure);
        
        // save links for later
        const linkCharity = (c_q.data['charity_info'].length>0) ? c_q.data['charity_info'][0] : undefined;
        const linkEvent   = (c_q.data[ 'event_info' ].length>0) ? c_q.data[ 'event_info' ][0] : undefined;
        setLink_charity(linkCharity)
        setLink_event(linkEvent)
        setGettingData(false);
    }

    // function called on page load to retrieve data initially
    useEffect(() => {
        get_data();
    }, [])

    // HTML output
    return (
        <center>
        {/* If we're loading data, don't render anything */}
        {!gettingData && 
        <div className='templatePage'>
            {/* Main Card on left that gives instance information */}
            <Card className='attributeCard'>
                <Card.Title style={{ fontSize: '6vh' }}>{locations_query['name']}</Card.Title>
                <center>
                <Card.Body style={{width: "450px"}}>
                    <div>
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                    <th>Attribute</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Longitude</td>
                                    <td>{locations_query['longitude']}</td>
                                </tr>
                                <tr>
                                    <td>Latitude</td>
                                    <td>{locations_query['latitude']}</td>
                                </tr>
                                <tr>
                                    <td>Population</td>
                                    <td>{locations_query['population']}</td>
                                </tr>
                                <tr>
                                    <td>Distance from you(miles)</td>
                                    <td>{gotGeolocationData && (69*Math.sqrt((locations_query['latitude'] - geolocation['latitude'])**2 + 
                                            (locations_query['longitude'] - geolocation['longitude'])**2)).toFixed(2)}
                                        {!gotGeolocationData && "No User Location"}</td>
                                </tr>
                                <tr>
                                    <td>Rating</td>
                                    <td>{locations_query['rating']}</td>
                                </tr>
                                <tr>
                                    <td>Safety</td>
                                    <td>{locations_query['safety']}</td>
                                </tr>
                                <tr>
                                    <td>Safety Text</td>
                                    <td>{locations_query['safety_text']}</td>
                                </tr>
                                <tr>
                                    <td>Budget</td>
                                    <td>{locations_query['budget']}</td>
                                </tr>
                                <tr>
                                    <td>Budget Text</td>
                                    <td>{locations_query['budget_text']}</td>
                                </tr>
                                <tr>
                                    <td>Covid</td>
                                    <td>{locations_query['covid']}</td>
                                </tr>
                                <tr>
                                    <td>Covid Text</td>
                                    <td>{locations_query['covid_text']}</td>
                                </tr>
                                <tr>
                                    <td>Roadgoat Link</td>
                                    <td><a href={locations_query['url']}>{locations_query['url']}</a></td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </Card.Body>
                </center>
            </Card>

            {/* Table on right side with map, images, and instance links */}
            <div style={{width: '650px', padding: '0px', float: 'left'}}>
                <Table className='mediaTable'>
                    {/* Google Maps Embed and Images */}
                    <tbody>
                        {/* Google Maps Embed */}
                        <tr>
                            <td>
                                <center>
                                <iframe  className='imgEmbed'
                                loading="lazy"
                                allowFullScreen
                                referrerPolicy="no-referrer-when-downgrade"
                                src  = {location} >
                                </iframe>
                                </center>
                            </td>
                            <td>
                                <center>
                                {locations_query['photo_url'] && <img src={locations_query['photo_url']} className='imgEmbed'/>}
                                {!locations_query['photo_url'] && <Card style={{marginTop: "0px"}} className='imgEmbed'> 
                                        <Card.Text style={{marginTop: "85px"}}>No Image</Card.Text></Card>}
                                </center>
                            </td>
                        </tr>

                        {/* Bottom row with links to other models */}
                        <tr>
                            {/* Charity Link */}
                            <td>
                                <center>
                                <p>Nearby Charity:</p>
                                <Nav.Link 
                                onClick={() => window.location.href="/charities/"+link_charity['id']}>
                                    {link_charity != undefined && 
                                    <Button variant="primary" className='linkButton'>
                                        {link_charity['name']}
                                    </Button>
                                    }
                                    {link_charity == undefined && "None"}
                                </Nav.Link>
                                </center>
                            </td>

                            {/* Event Link */}
                            <td>
                                <center>
                                <p>Nearby Event:</p>
                                <Nav.Link 
                                onClick={() => window.location.href="/events/"+link_event['id']}>
                                    {link_event != undefined && 
                                    <Button variant="primary" className='linkButton'>
                                        {link_event['name']}
                                    </Button>
                                    }
                                    {link_event == undefined && "None"}
                                </Nav.Link>
                                </center>
                            </td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </div>
        }
        </center>
    );

}

export default LocationInstance