const axios = require('axios');

function getCityEvent() {
    return axios.get(`https://api.giveandlive.me/location?page=-1`, {
        headers: {
        'Content-Type': 'application/json'
        }
      })
    .then(response => {
        return response['data']
    })
    .catch(error => console.log(error))
}

export { getCityEvent };
