import React, { PureComponent } from "react";
import Spinner from "./Spinner"
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

export default class Example extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    if (typeof this.props.data  !== 'undefined' && this.props.data.length !== 0) {
        return (
            <ResponsiveContainer width="100%" height={450}>
            <BarChart
              width={500}
              height={300}
              data={this.props.data[0].data}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="partyName" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="female" fill="#8884d8" />
              <Bar dataKey="male" fill="#82ca9d" />
            </BarChart>
          </ResponsiveContainer>
        );
    }
    else {
        return (
            <Spinner />
        );
    }
  }
}