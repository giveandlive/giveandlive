import React, { useEffect, useState, PureComponent } from 'react';
import LineChart from "./LineChart"

const StateChart = () => {
	const [billData, setBillData] = useState([]);

	useEffect(() => {
		async function fetchData() {
			let dictionary = {};
            let dictionary2 = {};

		let query = `https://api.justabill.info/states?page=` + 1;
		let response = await fetch(query);
		let json = await response.json();
		let numContent = json['total_results'];
		query = `https://api.justabill.info/states?page=` + 1 + `&per_page=` + numContent;
		response = await fetch(query);
		json = await response.json();
		let stuff = json['page']
        console.log(stuff)
		for (let entry in stuff) {
            dictionary[stuff[entry]['abbreviation']] = stuff[entry]['population']
            dictionary2[stuff[entry]['abbreviation']] = stuff[entry]['num_proposed_bills']
		}
		
		let allBillData = Object.keys(dictionary)

		const billData = [];
		allBillData.forEach((state) => {
			billData.push({
				name: state,
				population: dictionary[state] / (1000000),
                bills: dictionary2[state],
				fill: '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0'),
			});
		});
		setBillData([{ data: billData }]);
		}

		fetchData();
	}, []);

	return (
		  <LineChart data={billData}
		  />
	  );
}


export default StateChart