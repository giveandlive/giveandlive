import React, { useEffect, useState, PureComponent } from 'react';
import BarChart from "./BarChart"

const CPBarChart = () => {
	const [cpData, setCPData] = useState([]);

	useEffect(() => {
		async function fetchData() {
			let dictionary = {};
            let totalCongressPeople = 0;
        
			let query = `https://api.justabill.info/congresspeople?page=` + 1 + `&gender=F`;
			let response = await fetch(query);
			let json = await response.json();
			let numContent = json['total_results'];
			totalCongressPeople += numContent;
			query = `https://api.justabill.info/congresspeople?page=` + 1 + `&per_page=` + numContent + `&gender=F`;
			response = await fetch(query);
			json = await response.json();
			let stuff = json['page']
			for (let entry in stuff) {
					dictionary[stuff[entry]['party']] = (dictionary[stuff[entry]['party']] || 0) + 1;
			}

			query = `https://api.justabill.info/congresspeople?page=` + 1  + `&gender=M`;
			response = await fetch(query);
			json = await response.json();
			numContent = json['total_results'];
			totalCongressPeople += numContent;
			let dictionary2 = {}
			query = `https://api.justabill.info/congresspeople?page=` + 1 + `&per_page=` + numContent + `&gender=M`;
			response = await fetch(query);
			json = await response.json();
			stuff = json['page']
			for (let entry in stuff) {
				dictionary2[stuff[entry]['party']] = (dictionary2[stuff[entry]['party']] || 0) + 1;
			}
			
			dictionary['ID'] = (dictionary['ID'] || 0);

			let allBillData = Object.keys(dictionary)
			console.log(allBillData)
			let parties = ["D", "R", "ID"];
			let partyNames = {"D": "Democratic", "R": "Republican", "ID": "Independent"}
			const cpData = [];
			parties.forEach((party) => {
				cpData.push({
					party: party,
					partyName: partyNames[party],
					female: dictionary[party],
					male: dictionary2[party],
					total: totalCongressPeople,
				});
			});
			setCPData([{ data: cpData }]);
		}

		fetchData();
	}, []);

	return (
		  <BarChart
			// centerText={{ title: "Events by Genre"}}
			data={cpData}
		  />
	  );
}

export default CPBarChart