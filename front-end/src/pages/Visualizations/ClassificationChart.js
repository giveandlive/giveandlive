import React, { useEffect, useState, PureComponent } from 'react';
import RadarChart from "./RadarChart"

const CharityChart = () => {
	const [classificationData, setClassificationData] = useState([]);

	useEffect(() => {
		async function fetchData() {
			let dictionary = {};
            let totalCharityCount = 0;
        
		let query = `https://api.giveandlive.me/charity?classifications&page=-1`;
		let response = await fetch(query);
		let json = await response.json();
		let numContent = json['num'];
		totalCharityCount += numContent;
		let stuff = json['content']
        console.log(stuff)
		for (let entry in stuff) {
			dictionary[stuff[entry]['classification']] = (dictionary[stuff[entry]['classification']] || 0) + 1;
		}

		let allBillData = Object.keys(dictionary)
		console.log(allBillData)
        let charityClassification = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11];
        let charityClassificationNames = ["Animals", "Arts, Culture, Humanities", "Community Development", "Education", "Environment", "Health", "Human and Civil Rights", "Human Services", "International", "Research and Public Policy", "Religion"];
		const classificationData = [];
        console.log(totalCharityCount)
		charityClassification.forEach((charity) => {
			console.log(charityClassificationNames[charity])
			classificationData.push({
				charity: charity,
                partyName: charityClassificationNames[charity - 1],
				female: dictionary["" + charity + ""],
                total: totalCharityCount,
			});
		});
		setClassificationData([{ data: classificationData }]);
		}

		fetchData();
	}, []);

	return (
		  <RadarChart data={classificationData}
		  />
	  );
}

export default CharityChart