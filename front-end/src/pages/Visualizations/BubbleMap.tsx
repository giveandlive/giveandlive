import React from 'react';
import * as d3 from "d3";
import * as topojson from "topojson-client"

export default class BubbleMap extends React.Component<any, any> {
    constructor(props) {
        super(props);

    }
    async renderChart(cities: any) {
        const svg = d3.select("#my_dataviz")
        
        let countyURL = 'https://cdn.freecodecamp.org/testable-projects-fcc/data/choropleth_map/counties.json'
        let countyData
        
        d3.json(countyURL).then(
            (data, error) => {
                if(error){
                    console.log("error")
                }else{
                    countyData = topojson.feature(data, data.objects.counties).features        
                    drawMap()
                    d3.selection.prototype.moveToBack = function() {  
                        return this.each(function() { 
                            var firstChild = this.parentNode.firstChild; 
                            if (firstChild) { 
                                this.parentNode.insertBefore(this, firstChild); 
                            } 
                        });
                    };
                }
                

            });
        let canvas = d3.select('#my_dataviz')
        let tooltip = d3.select('#tooltip')
        
        let drawMap = () => {
            canvas.selectAll('path')
                .data(countyData)
                .enter()
                .append('path')
                .attr('d', d3.geoPath())
                .attr('class', 'county')
                .attr('fill', (countyDataItem) => {
                    d3.select(this).moveToBack();
                    return 'black'
                })
                .attr('data-fips', (countyDataItem) => {
                    d3.select(this).moveToBack();
                    return countyDataItem['id']
                })
                .on('mouseover', (countyDataItem)=> {
                    tooltip.transition()
                        .style('visibility', 'visible')
                })
                .on('mouseout', (countyDataItem) => {
                    tooltip.transition()
                        .style('visibility', 'hidden')
                })
            }
        const projection = d3.geoMercator()
        .center([-97,40])                // GPS of location to zoom on
        .scale(940)                       // This is like the zoom
        // .translate([ cities.width , cities.height])

        Promise.all([
        d3.json("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/us_states_hexgrid.geojson.json")
        ]).then(function() {
            let data = cities
    
            // Create a color scale
            const color = d3.scaleOrdinal()
                .domain(data.map(d => d.country))
                .range(d3.schemePaired);
        
            // Add a scale for bubble size
            const valueExtent = d3.extent(data, d => +d.value)
            const size = d3.scaleSqrt()
            .domain(valueExtent)  // What's in the data
            .range([ 1, 15])  // Size in pixel

            d3.selection.prototype.moveToFront = function() {  
                return this.each(function(){
                  this.parentNode.appendChild(this);
                });
              };

            svg
            .selectAll("myCircles")
            .data(data.sort((a,b) => +b.value - +a.value).filter((d,i) => i<1000))
            .join("circle")
            .attr("cx", d => projection([+d.long, +d.lat ])[0])
            .attr("cy", d => projection([+d.long, +d.lat])[1])
            .attr("r", d => size(+d.value))
            .style("fill", d => color(d.country))
            .attr("stroke", d=> {if (d.value>2000) {return "black"} else {return "none"}  })
            .attr("stroke-width", 1)
            .attr("fill-opacity", .9)
        })

    }

    async componentDidMount() {

        await this.renderChart(this.props.cities);

    }

    render() {
        return (
            <div>
                <div>
                    <svg id="my_dataviz" width="1000" height="1000"></svg>
                </div>
            </div>
        );
    }

}

