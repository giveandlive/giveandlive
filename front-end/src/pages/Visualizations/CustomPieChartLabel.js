import React from "react";
// import Spinner from "./Spinner"
const RADIAN = Math.PI / 180;

const renderCustomizedLabel = (props) => {
  console.log("rendered");
  const {
    cx,
    cy,
    midAngle,
    outerRadius,
    fill,
    payload,
    percent,
    value,
    centerText
  } = props;
  const sin = Math.sin(-RADIAN * midAngle);
  const cos = Math.cos(-RADIAN * midAngle);
  const sx = cx + (outerRadius + 10) * cos;
  const sy = cy + (outerRadius + 10) * sin;
  const mx = cx + (outerRadius + 30) * cos;
  const my = cy + (outerRadius + 30) * sin;
  const ex = mx + (cos >= 0 ? 1 : -1) * 30;
  const ey = my;
  const textAnchor = cos >= 0 ? "start" : "end";

  if (percent > 0.015) {
    return (
        <g>
             <text x={cx} y={cy} textAnchor="middle" fill={'black'}>
            {centerText.title}
          </text>
  
          <path
            d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
            stroke={fill}
            fill="none"
          />
    
          <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
          <text
            style={{ fontWeight: "bold" }}
            x={ex + (cos >= 0 ? 1 : -1) * 12}
            y={ey}
            textAnchor={textAnchor}
            fill={fill}
          >
            {payload.name}
          </text>
        </g>
      );
  }
  else {
      return(
          <g>
            {/* <path
                d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
                stroke="none"
                fill="white"
            /> */}
          </g>      
      );
  }
  
};

const CustomPieChartLabel = React.memo(renderCustomizedLabel);

export default CustomPieChartLabel;
