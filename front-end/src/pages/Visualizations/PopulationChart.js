import React, { useEffect, useState } from 'react';
import PopulationBar from "./PopulationBar"

const PopulationChart = () => {
	const [genreData, setGenreData] = useState([]);

	useEffect(() => {
		async function fetchData() {
			let populations = {};
            let totalLocations = 0;
            let minPopulation = Number.MAX_SAFE_INTEGER;
            let maxPopulation = 0;

            for (let i = 1; i <= 8; i++) {
                const query = `https://api.giveandlive.me/location?page=` + i;
                let response = await fetch(query);
                let json = await response.json();
                let stuff = json['content']
                for (let entry in stuff) {
                    totalLocations++;
                    // console.log(stuff[entry]['population']);
                    if (stuff[entry]['population'] > maxPopulation) {
                        maxPopulation = stuff[entry]['population'];
                        // console.log("new max ")
                    }
                    if (stuff[entry]['population'] < minPopulation) {
                        minPopulation = stuff[entry]['population'];
                    }
                    populations[stuff[entry]['population']] = (populations[entry['population']] || 0) + 1;
                }
            }
    
            let allPopulations = Object.keys(populations)
                     
            let count = [0,0,0,0,0,0,0,0]
            let limit = [1000, 5000, 10000, 50000, 100000, 250000, 500000, 1000000]
            let divider = 8
            allPopulations.forEach((population) => {
                for (let i = 1; i <= divider; i++) {
                    if (limit[i - 1] >= population) {
                        count[i - 1]++;
                        break;
                    }
                }
            });

            const genreData = [];

            // percentage of populations that are OVER the limit
            let nameData = ["0-1000", "1001-5000", "5001-10000", "10001-50000", "50001-100000", "100001-250000", "250001-500000", "500001-1 mil"];
            let colorScheme = ["#342f95", "#504ac6", "#8884d8", "#83a6ed", "#8dd1e1", "#82ca9d", "#a4de6c", "#cbecaa"]
            for (let i = 0; i < divider; i++) {
                genreData.push({
                    name: nameData[i],
                    value: count[i],
                    fill: colorScheme[i],
                });
            }
        
            setGenreData([{ data: genreData }]);
            }

            fetchData();
	}, []);
    

	return (
    <PopulationBar
        data={genreData}
    />
	);
};


export default PopulationChart