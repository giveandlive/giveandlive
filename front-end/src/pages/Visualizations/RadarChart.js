import React, { PureComponent } from "react";
import { Radar, RadarChart, PolarGrid, Legend, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';
import Spinner from "./Spinner"

export default class Example extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
        if (typeof this.props.data  !== 'undefined' && this.props.data.length !== 0) {
            return (
                <ResponsiveContainer width='100%' height={550}>
                <RadarChart outerRadius={200}  data={this.props.data[0].data}>
                   <PolarGrid />
                    <PolarRadiusAxis angle={30} domain={[0, 50]} />
                    <PolarAngleAxis dataKey="partyName" />,
                    <Radar name="Female" dataKey='female' stroke="#20338F" fill="#20338F" fillOpacity={0.6} />,
                </RadarChart>
                </ResponsiveContainer>
              );
        }
        else {
            return (
                <Spinner />
            );
        }
      
  }
}