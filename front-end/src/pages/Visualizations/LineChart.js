import React, { PureComponent } from "react";
import { LineChart, Label, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import Spinner from "./Spinner"
import styles from "./LineChart.css"

export default class Example extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
        if (typeof this.props.data  !== 'undefined' && this.props.data.length !== 0) {
            return (
                <ResponsiveContainer width='90%' height={550}>
                <LineChart
                  width={500}
                  height={300}
                  data={this.props.data[0].data}
                  margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 10,
                  }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="name" />
                  <YAxis yAxisId="left" label={{ value: "Number of Bills" , angle: -90, position: 'left' }}>                   
                    {/* <Label  /> */}
                  </YAxis>
                  <YAxis yAxisId="right" orientation="right" label={{ value: "Population in Millions" , angle: 90, position: 'right'}} />
                  <Tooltip />
                  <Legend />
                  <Line yAxisId="left" type="monotone" dataKey="bills" stroke="#8884d8" activeDot={{ r: 8 }} />
                  <Line yAxisId="right" type="monotone" dataKey="population" stroke="#82ca9d" />
                  {/* <LabelList dataKey="name" position="top" /> */}
                </LineChart>
              </ResponsiveContainer>
              );
        }
        else {
            return (
                <Spinner />
            );
        }   
  }
}