import React, { PureComponent } from "react";
import { PieChart, Pie, Cell, Tooltip, ResponsiveContainer } from "recharts";
import CustomPieChartLabel from "./CustomPieChartLabel";

export default class Example extends PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    // console.log(this.props.centerText);
    return (
        <ResponsiveContainer width='100%' height={550}>
			
		<PieChart centerText={{ title: this.props.centerText}}  data={this.props.data}>
		
			<Tooltip />
			{this.props.data.map((s) => (
			<Pie
				data={s.data}
				dataKey='value'
				cx='50%'
				cy='50%'
				innerRadius={'55%'}
				outerRadius={'85%'}
				labelLine={false}
				fill='white'
				label={<CustomPieChartLabel  centerText={this.props.centerText}/>}
			/>
			))}			
		</PieChart>
		</ResponsiveContainer>
    );
  }
}
