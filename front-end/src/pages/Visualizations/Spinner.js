import React, { PureComponent } from "react";
import { Spin } from "antd"
import 'antd/dist/antd.css';

const messages = [
	"Loading...",
]

const styles = {
	margin: "auto",
	display: "block",
}

/**
 * Returns an Ant Design spinner component with a randomized loading message
//  */
export default class Spinner extends PureComponent {
	render(){
		const message =
		messages[Math.floor(Math.random() * messages.length)] + "..."
    	console.log("in spinner")
		return(
			<Spin style={styles} tip={message} size="large" />
		);
	}
}
// export default () => <Spin />;
