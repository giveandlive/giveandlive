import React, { useEffect, useState } from 'react';
import { getCityEvent } from './BubbleMapHelper.tsx'
import BubbleMap from './BubbleMap.tsx';

/**
 * 
 * US map derived from https://www.notion.so/Visualize-Data-with-a-Choropleth-Map-9d91d46e78d4406abc6a0d36f9e089dc#2bcf9d3b88b14178a74525c3a490d5e7
 */

function CharitiesChart(props: any) {
    const [cities, setCities] = useState([]);
    const [citiesLoading, setCitiesLoading] = useState(false);

    useEffect(() => {
        setCitiesLoading(true)
        getCityEvent()
        .then(response => {
            var data = response['content']
            var cities = {}
            var result = []

            for (var i = 0; i < data.length; i++) {
                var id = data[i]['id']
                console.log(data[i]['name'])
                if (id in cities) {
                    cities[id]['value'] += 1
                }
                else {
                    cities[id] = {
                        'City': data[i]['name'],
                        'lat': data[i]['latitude'],
                        'long': data[i]['longitude'],
                        'country': 'United States',
                        'state': data[i]['state'],
                        'value': 1
                    }
                }
            }
            var keys = Object.keys(cities)
            for (var i = 0; i < keys.length; i++) {
                result.push(cities[keys[i]])
            }
            setCities(result)
            setCitiesLoading(false)
        })
    }, []);

    return (
        <BubbleMap cities={cities} width='100%' height= '100%'/>
    );
}

export default CharitiesChart;
