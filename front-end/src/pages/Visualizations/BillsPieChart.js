import React, { useEffect, useState, PureComponent } from 'react';
import CustomPieChart from "./CustomPieChart"

const BillsPieChart = () => {
	const [billData, setBillData] = useState([]);

	useEffect(() => {
		async function fetchData() {
			let dictionary = {};
			let query = `https://api.justabill.info/bills?page=` + 1;
			let response = await fetch(query);
			let json = await response.json();
			let numContent = json['total_results'];
			query = `https://api.justabill.info/bills?page=` + 1 + `&per_page=` + numContent;
			response = await fetch(query);
			json = await response.json();
			let stuff = json['page']
			for (let entry in stuff) {
				dictionary[stuff[entry]['committees']] = (dictionary[stuff[entry]['committees']] || 0) + 1;
			}
		
			let allBillData = Object.keys(dictionary)

			const billData = [];
			allBillData.forEach((committee) => {
				billData.push({
					name: committee,
					value: dictionary[committee],
					fill: '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0'),
				});
			});
			setBillData([{ data: billData }]);
		}

		fetchData();
	}, []);

	return (
		  <CustomPieChart
			centerText={{ title: "Bills by Committee"}}
			data={billData}
		  />
	  );
}

export default BillsPieChart