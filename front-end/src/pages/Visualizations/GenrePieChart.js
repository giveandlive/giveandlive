import React, { useEffect, useState, PureComponent } from 'react';
import CustomPieChart from "./CustomPieChart"
import Spinner from "./Spinner"
import {useNavigate} from "react-router-dom"

const GenrePieChart = () => {

	const [genreData, setGenreData] = useState([]);
	const[loading, setLoading] = useState(false)
	const navigate = useNavigate()

	useEffect(() => {
		async function fetchData() {
			setLoading(true)

			let dictionary = {};

			const query = `https://api.giveandlive.me/event?page=` + 1;
			let response = await fetch(query);
			let json = await response.json();
			let numContent = json['num'];
			let numPages = numContent/20;
			for (let i = 1; i <= numPages; i++) {
				const query = `https://api.giveandlive.me/event?page=` + i;
				let response = await fetch(query);
				let json = await response.json();
				let stuff = json['content']
				for (let entry in stuff) {
					if (stuff[entry]['genre'] == "NaN" || stuff[entry]['genre'] == "Undefined") {
						dictionary['Other'] = (dictionary['Other'] || 0) + 1;
					}
					else {
						dictionary[stuff[entry]['genre']] = (dictionary[stuff[entry]['genre']] || 0) + 1;
					}
				}

			}
			
			let allGenreData = Object.keys(dictionary)

			const genreData = [];
			allGenreData.forEach((genre) => {
				genreData.push({
					name: genre,
					value: dictionary[genre],
					fill: '#'+(Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0'),
				});
			});
			setGenreData([{ data: genreData }]);
			setLoading(false)
		}

		fetchData();
	}, []);

	if (loading) {
		return <Spinner />
	}

	return (
		  <CustomPieChart
			centerText={{ title: "Events by Genre"}}
			
			onClick={({ activePayload }) => {
				const id = activePayload?.pop().payload.genre
				if (id) {
					console.log("event?search=" + {id})
					navigate(`event?search=${id}`)
				}
			}}

			data={genreData}
		  />
	  );
}


export default GenrePieChart