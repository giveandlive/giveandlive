import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Table, Stack } from 'react-bootstrap';
import Paginate from './Pagination.js'
import axios from 'axios'
import './ModelPages.css';
import Select from 'react-select'
import { updateFilters } from './Filtering.js'
import Highlighter from "react-highlight-words"

// Event model element
const Events = ({ fetchPage }) => {

    const [currentPage, setCurrentPage] = useState(1); /* hook for page */
    const [events_query, setEvents_query] = useState({'content': [], 'num':0}); /* hook for API result */
    const [gettingData, setGettingData] = useState(false); /* hook for tracking when waiting for API */
    const [filterChoices, setFilterChoices] = useState([]); /* hook for filter choices */
    const [filterQueries, setFilterQueries] = useState([]); /* hook for filter queries */
    const [searchQuery, setSearchQuery] = useState(""); /* hook for search query */
    const [sortquery, setSortquery] = useState(""); /* hook for sort query */
    const [filtering, setFiltering] = useState(null); /* hook for tracking when filtering */
    const [searching, setSearching] = useState(null); /* hook for tracking when searching */
    const [sorting, setSorting] = useState(null); /* hook for tracking when sorting */

    /* 
     * helper function to construct API call for other functions. Code also handles
     * discrepancies from previous page loads from unupdated hooks.
     */
    function getQuery(page, query, ignore) {
        let url = `https://api.giveandlive.me/event`;
        url = url + `?page=${page}`

        // ignore handles overriding previous queries
        if (ignore == undefined) {
            ignore = ""
        }
        if (query == undefined) {
            query = "";
        }

        // if sort is up to date, then sort by the given query or the hook
        if (ignore !== 'sort') {
            if (query.includes("sort")) {
                url = url + '&' + query;
            }
            else if (sortquery !== "" && sortquery != null) {
                url = url + `&sort=${sortquery}`
            }
        }

        // if filter is up to date, then filter by the given query or the hook
        if (ignore !== 'filter') {
            if (query.includes("filter")) {
                url = url + '&' + query.replace("filter=", "");
            }
            else if (filtering != null && filtering !== "") {
                let updates = updateFilters(filtering[0], filtering[1], filterQueries, page)
                url = url + `&${updates.linkParams.split("&")[1]}`
            }
        }

        // if search is up to date, then search by the given query or the hook.
        if (ignore !== 'search') {
            if (query.includes("search")) {
                url = url + '&' + query;
            }
            else if (searching !== null && searching !== undefined && searching !== "") {
                if (searching.includes('search=')) {
                    url = url + `&${searching}`
                } else {
                    url = url + `&search=${searching}`
                }
            }
        }
        return url;
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the event model page, specifically for searching.
     */
    const fetchEvents = async(page, query, ignore) =>  {
        setGettingData(true);
        setSearching(query);
        const url = getQuery(page, query);
        const res = await axios.get(url);
        setEvents_query(res.data);
        setGettingData(false)
    }

    /* 
     * function to call the giveandlive API endpoint corresponding to 
     * the event model page, for general purpose.
     */
    const get_data = async(page, query, ignore) => {
        setGettingData(true);
        const url = getQuery(page, query, ignore);
        const c_q = await axios.get(url);
        setEvents_query(c_q.data);
        setGettingData(false);
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the filter choices, used for dynamically populating the filter box options.
     */
    const getFilterChoices = async() => {
        // call API endpoint for filter options for event model page
        const status = await axios.get('https://api.giveandlive.me/event?statuses')
        const genre = await axios.get('https://api.giveandlive.me/event?genres')
        
        // retrieve just the status and genre values and sort them
        let statusCheck = status.data.content.map((item) => item.status)
        statusCheck.sort(function (first, second) {return first - second})
        let genreCheck = genre.data.content.map((item) => item.genre)
        genreCheck.sort(function (first, second) {return first - second})
        
        // store the choices for use by React
        let statusChoices = [...new Set(statusCheck)]
        statusChoices = statusChoices.map((item) => {
            return {value: 'status' + item, type: 'status', label: item}
        })
        let genreChoices = [...new Set(genreCheck)]
        genreChoices = genreChoices.map((item) => {
            return {value: 'genre' + item, type: 'genre', label: item}
        })

        // store the default None values and store in the hook
        statusChoices.unshift({value: 'statusNone', type: 'status', label: 'None'})
        genreChoices.unshift({value: 'genreNone' , type: 'genre', label: 'None'})
        setFilterChoices([statusChoices, genreChoices])
    }

    /* 
     * function to handle filtering selection and retrieve filtered data
     */
    const getFilteredPage = (page, type, choiceObj) => {
        setFiltering([type, choiceObj]);
        let updates = updateFilters(type, choiceObj, filterQueries, page)
        setFilterQueries([...updates.choices]);
        let query = updates.linkParams.split("&")[1];
        let ignore = ""
        if (query == undefined) {
            ignore = 'filter'
            query = ""
        } else {
            query = 'filter=' + query;
        }
        get_data(page, query, ignore);
    }

    /* 
     * function to handle sorting selection and retrieve sorted data
     */
     const get_sorted_data = async(page, sort) => {
        setGettingData(true);
        setSorting(true);
        const url = getQuery(page, 'sort='+sort);
        const c_q = await axios.get(url);

        setEvents_query(c_q.data);
        setGettingData(false);
    }

    /* 
     * load default data and filter options on initial page load
     */
    useEffect(() => {
        get_data(1, "", "");
        getFilterChoices();
    }, [])

    /* 
     * function to paginate the model page, setting the page hook
     * and calling the correct function to reload the page
     */
    const paginate = (num) => {
        setCurrentPage(num);
        if (searching) {
            fetchEvents(num, searching);
        } else if (filtering) {
            getFilteredPage(num, filtering[0], filtering[1]);
        } else if (sorting) {
            get_sorted_data(num, sortquery);
        } else {
            get_data(num);
        }
    }

    /* 
     * handler function to simply reload the data with the correct query
     */
    const fetchSearchResults = () => {
        if (searchQuery!=null) {
            fetchEvents(1, `search=${searchQuery}`)
        } else {
            fetchEvents(1, '');
        }
    }

    /* 
     * handler function to search on Enter key press
     */
    const handleKeyDown = (event) => {
        if (event.key == "Enter") {
            fetchSearchResults()
        }
    }

    /* 
     * JS object to hold the sorting options
     */
    const features = [
        { label: "Name (A-Z)", value: 2 },
        { label: "Name (Z-A)", value: 1 },
        { label: "Date (Desc)", value: 3 },
        { label: "Date (Asc)", value: 4 },
        { label: "Time (Asc)", value: 6 },
        { label: "Time (Desc)", value: 5 },
        { label: "Price (Asc)", value: 8 },
        { label: "Price (Desc)", value: 7 },
    ];

    /* 
     * handler function to handle selection of a sort option, and
     * eventually retrieve the sorted data
     */
    const handleSelect=(e)=>{
            let sort = "";
            sort += e["label"].toLowerCase().split(" ")[0].trim()
            if (e["value"] % 2 == 1) {
                sort += "-descending"
            } else {
                sort += "-ascending"
            }
            setSortquery(sort);
            get_sorted_data(currentPage, sort);    
    }

    // HTML output
    return (
    <div className='outer'>
        <h1>Events</h1>
        {/* Holds the options to change rendered data */}
        <div>
            {/* box for status filter */}
            <div style={{ width:"200px", height:'100px', float:'left', marginRight: '15px'}}>
                <h6>Status Filter</h6>
                <Select options={filterChoices[0]} defaultValue="Name" isSearchable={true} 
                onChange={(choice) => getFilteredPage(1, "status", choice)} isClearable={true}/>
            </div>
            {/* box for genre filter */}
            <div style={{ width:"200px", marginRight: '15px', height:'100px', float:'left'}}>
                <h6>Genre Filter</h6>
                <Select options={filterChoices[1]} defaultValue="Name" isSearchable={true} 
                onChange={(choice) => getFilteredPage(1, "genre", choice)} isClearable={true}/>
            </div>
            {/* box for sorting */}
            <div style={{ width:"200px", height:'100px', float:'left', marginRight: '15px'}}>
                <h6>Sort</h6>
                    <Select options={ features }
                            onChange={handleSelect}
                            onSelect={handleSelect}/>
            </div>
            {/* box for searching */}
            <div style={{ height:'100px',  float:'left'}}>
                <h6>Events Search</h6>
                <input style={{float:'left', paddingLeft:'15px', width: '200px', height: '38px'}} 
                    type="text" onChange={e => setSearchQuery(e.target.value)} onKeyPress={handleKeyDown}
                    placeholder="Enter query"/>
                <Button style={{ backgroundColor: '#00008b', float:'left', marginLeft:'15px'}} 
                    variant="primary" type="submit" onClick={() => fetchSearchResults()}>
                    Submit
                </Button>
            </div>
        </div>
        {/* handles the end of the float */}
        <div style={{clear:'both'}}></div>
        {/* Dynamically renders the current page of data */}
        {gettingData && <h3>Loading...</h3>}
        {!gettingData &&
        <Table striped bordered hover>
        <thead>
            <tr>
            <th>Event Name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Status</th>
            <th>Genre</th>
            <th>Minimum Price</th>
            </tr>
        </thead>
        <tbody>
            {/* for each event object, render the following */}
            {events_query['content'].map((event, idx) => (
                <tr key={idx} className='TableRow'
                    onClick={() => window.location.href = "/events/"+event['id']}>
                <td><Highlighter
                        searchWords={[searchQuery]}
                        autoEscape={true}
                        textToHighlight={event['name']}
                    /></td>
                <td>{event['startDate']}</td>
                <td>{event['startTime']}</td>
                <td>{event['status']}</td>
                <td><Highlighter
                        searchWords={[searchQuery]}
                        autoEscape={true}
                        textToHighlight={event['genre']}
                    /></td>
                <td>{event['priceMin'] != -1 && event['priceMin']}
                    {event['priceMin'] == -1 && "No Data"}
                </td>
                </tr>
            ))}
            
        </tbody>
        </Table>
        }
        {/* Render information about this page of data */}
        <Stack gap={0}>
        <div className="center"><h5>Number of entries: {events_query['num']}</h5></div>
        <div className="center"><h5>Page: {currentPage}/{Math.ceil(events_query['num']/20)}</h5></div>
        <div className="center">
            <Paginate totalItems={events_query['num']} itemsPerPage={20} paginate={paginate} />
        </div>
        </Stack>       

    </div>
    );
}
  
export default Events