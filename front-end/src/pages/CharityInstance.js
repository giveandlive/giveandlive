import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import { Card, Button, Nav, Table } from 'react-bootstrap';
import axios from 'axios'
import './CharityInstance.css';

// Charity Instance element
const CharityInstance = () =>  {
    
    const params = useParams();   /* stores the charityID */

    const [charities_query, setCharities_query] = useState({}); /* Hook for API result */
    const [gettingData, setGettingData] = useState(false); /* Hook for when waiting for API call to return */
    const [location, setlocation] = useState(""); /* Hook to keep track of Charity location for Google Maps */
    const [link_location, setLink_location] = useState({'id':0, 'name': 'none'}); /* Hook for link to LocationInstance */
    const [link_event, setLink_event] = useState({'id':0, 'name': 'none'}); /* Hook for link to EventInstance */

    const api_key = process.env.REACT_APP_API_KEY; /* API Key for Google Maps */
    
    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the requested charity instance from useParams()
     */
    const get_data = async() => {

        setGettingData(true);

        // call api.giveandlive.me for charityInstance info
        const url = 'https://api.giveandlive.me/charity/' + params.charityId; 
        const c_q = await axios.get(url);
        setCharities_query(c_q.data);

        // use API key and charity instance location data to construct 
        // API call to Google Maps API
        const mapurl = "https://www.google.com/maps/embed/v1/place?key=" + api_key + "&q="+
        String(c_q.data['address']).trim().replaceAll(" ", "+") + "," + 
        String(c_q.data[ 'city'  ]).trim().replaceAll(" ", "+") + "," +
        String(c_q.data[ 'state' ]).trim().replaceAll(" ", "+");
        setlocation(mapurl);

        // save links for later
        setLink_location(c_q.data['location_info'])
        setLink_event(c_q.data['event_info'])

        setGettingData(false);

    }

    // function called on page load to retrieve data initially
    useEffect(() => {
        get_data();
    }, [])

    // HTML output
    return (
        
        <center className="mainCenter">
        {/* If we're loading data, don't render anything */}
        {!gettingData &&
            <div className='charityInstance'>

            {/* Main Card on left that gives instance information */}
            <Card className='attributeCard'>
                <Card.Title style={{ fontSize: '6vh' }}>{charities_query['name']}</Card.Title>
                <center>
                <Card.Body>
                    <div>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Attribute</th>
                                <th>Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Address</td>
                                <td>{charities_query['address']}</td>
                            </tr>
                            <tr>
                                <td>Zip Code</td>
                                <td>{charities_query['zipCode']}</td>
                            </tr>
                            <tr>
                                <td>City</td>
                                <td>{charities_query['city']}</td>
                            </tr>
                            <tr>
                                <td>State</td>
                                <td>{charities_query['state']}</td>
                            </tr>
                            <tr>
                                <td>Overall Rating</td>
                                <td>{charities_query['overallRating']}</td>
                            </tr>
                            <tr>
                                <td>Financial Rating</td>
                                <td>{charities_query['financialRating']}</td>
                            </tr>
                            <tr>
                                <td>Mission Statement</td>
                                <td>{charities_query['mission']}</td>
                            </tr>
                            <tr>
                                <td>Tagline</td>
                                <td>{charities_query['tagLine']}</td>
                            </tr>
                            <tr>
                                <td>charityEIN</td>
                                <td>{charities_query['charityEIN']}</td>
                            </tr>
                            <tr>
                                <td>Classification</td>
                                <td>{charities_query['classification']}</td>
                            </tr>
                            <tr>
                                <td>URL</td>
                                <td><a href={charities_query['url']}>{charities_query['url']}</a></td>
                            </tr>
                            
                        </tbody>
                    </Table>
                    </div>
                </Card.Body>
                </center>
            </Card>
            
            {/* Table on right side with map, images, and instance links */}
            <div style={{width: '650px', padding: '0px', float: 'left'}}>
                <Table className='mediaTable'>
                    <tbody>
                        {/* Google Maps Embed and Images */}
                        <tr>
                            {/* Google Maps Embed */}
                            <td>
                                <center>
                                <iframe  className='imgEmbed'
                                loading="lazy"
                                allowFullScreen
                                referrerPolicy="no-referrer-when-downgrade"
                                src  = {location} >
                                </iframe>
                                </center>
                            </td>

                            {/* Images */}
                            <td>
                                <center>
                                <Table style={{padding: '0px', marginTop: '50px'}} >
                                    {/* Top Row with rating image that spans two cols */}
                                    <tr>
                                        <td colSpan={2}>
                                            <center>
                                            <img style={{height: '20px', width: '90px'}} src={charities_query['ratingImage']}></img>
                                            <p style={{marginTop: '15px'}}>Rating</p>
                                            </center>
                                        </td>
                                    </tr>
                                    {/* Bottom Row with two images in two columns */}
                                    <tr style={{padding: '0px'}}>
                                        <center>
                                        <td style={{padding: '0px', margin: '0px'}}>
                                            <img style={{height: '100px', width: '95px', paddingRight: '5px'}} src={charities_query['categoryImage']}></img>
                                            Category
                                        </td>
                                        <td>
                                            <img style={{height: '100px', width: '95px'}} src={charities_query['causeImage']}></img>
                                            Cause
                                        </td>
                                        </center>
                                    </tr>
                                </Table>
                                </center>
                            </td>
                        </tr>

                        {/* Bottom row with links to other models */}
                        <tr>
                            {/* Location Link */}
                            <td>
                                <center>
                                <p>Nearby Location:</p>
                                <Nav.Link 
                                onClick={() => window.location.href="/locations/"+link_location['id']}>
                                        {link_location != undefined && 
                                        <Button variant="primary" className='linkButton'> {link_location['name']} </Button>
                                        }
                                        {link_location == undefined && "None"}
                                </Nav.Link>
                                </center>
                            </td>

                            {/* Event Link */}
                            <td>
                                <center>
                                <p>Nearby Event:</p>
                                <Nav.Link 
                                onClick={() => window.location.href="/events/"+link_event['id']}>
                                    {link_event != undefined && 
                                    <Button variant="primary" className='linkButton'>
                                        {link_event['name']}
                                    </Button>
                                    }
                                    {link_event == undefined && "None"}
                                </Nav.Link>
                                </center>
                            </td>
                        </tr>
                    </tbody>
                    
                </Table>
                </div>
            </div>
        } 
        </center>
    );

}

export default CharityInstance