import React from 'react'

/*
 * helper function to handle the filter object selection for the model pages
 */
export const updateFilters = (type, choiceObj, filterQueries, currentPage) => {
    let updatedChoices = []
    for (let i = 0; i < updatedChoices.length; i++) {
        if (updatedChoices[i].type === type) {
            updatedChoices = [...filterQueries]
            updatedChoices = updatedChoices.splice(i, 1)
        }
    }
    let params = "" + currentPage
    if (choiceObj !== null && choiceObj.label !== "None") {
        updatedChoices = [...updatedChoices, choiceObj]
        
    }
    for (let j = 0; j < updatedChoices.length; j++) {
        params = params + "&" + updatedChoices[j].type + "=" + updatedChoices[j].label
    }
    return {linkParams: params.toLowerCase(), choices: updatedChoices}
}
