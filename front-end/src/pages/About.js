import React, { useEffect, useState } from 'react'
import { Row, Col, Card, Button, Table, ListGroup, Tabs, Tab, Stack} from 'react-bootstrap'
import MalithyWimalasooriyaImg from "../images/MalithyWimalasooriya.jpg"
import VarunNayakImg from "../images/VarunNayak.jpeg"
import TakaKoutsomitopoulosImg from "../images//TakaKoutsomitopoulos.png"
import BruceNguyenImg from "../images//BruceNguyen.jpeg"
import SahranHashimImg from "../images//SahranHashim.jpg"
import gitlabIMG from "../images/gitlabIMG.png"
import postmanIMG from "../images/postmanIMG.png"
import reactIMG from "../images/react.png"
import reactrouterIMG from "../images/reactrouter.png"
import reactbootstrapIMG from "../images/reactbootstrap.png"
import awsIMG from "../images/aws.png"
import flaskIMG from "../images/flask.png"
import sqlalchemyIMG from "../images/sqlalchemy.jpeg"
import jestIMG from "../images/jest.png"
import marshmallowIMG from "../images/marshmallow.png"
import postreSQLIMG from "../images/postgres.png"
import seleniumIMG from "../images/selenium.png"
import namecheapIMG from "../images/nc1.jpeg"
import dockerIMG from "../images/docker.png"
import cnIMG from "../images/cn.jpeg"
import ticketmasterIMG from "../images/ticketmaster.png"
import rgIMG from "../images/roadgoat.jpeg"
import gmIMG from "../images/gm.png"
import './About.css'
import axios from 'axios';

/*
 * JS object to hold data about all the team members
 */
const teamMembers = [
    {
        name: "Malithy Wimalasooriya",
        username: "malithyw",
        email: "malithyw@utexas.edu",
        picture: MalithyWimalasooriyaImg,
        role: "Full-Stack & Phase IV Leader",
        bio:
            "I’m a third year CS Major minoring in Business. I'm from Pearland, TX, and spend my free time playing volleyball, watching thriller/horror movies, and going out to eat with my friends.",
        commits: 0,
        issues: 0,
        tests: 14,
    },
    {
        name: "Bruce Nguyen",
        username: "brucedatnguyen",
        email: "brucenguyen45@utexas.edu",
        picture: BruceNguyenImg,
        role: "Full-Stack & Phase I Leader",
        bio:
            "Hi there! I am a 3rd year Computer Science student with a minor in Business Foundations. I am originally from Houston, Texas, and I enjoy jogging, kayaking, and spending time with my family and friends.",
        commits: 0,
        issues: 0,
        tests: 15,
    },
    {
        name: "Sahran Hashim",
        username: "sahranhashim99",
        email: "sahranhashim99@gmail.com",
        picture: SahranHashimImg,
        role: "Back-End & Phase III Leader",
        bio:
            "I'm a senior CS major with a minor in business foundations. I'm originally from Sugar Land, Tx but I live permamently in Austin now. In my free time I like to dance, play basketball, and play video games with my friends.",
        commits: 0,
        issues: 0,
        tests: 12,
    },
    {
        name: "Varun Nayak",
        username: "varunn5",
        email: "varun.d.nayak@gmail.com",
        picture: VarunNayakImg,
        role: "Back End",
        bio:
            "Hi, I am a 3rd year computer science major at the University of Texas at Austin. I am from Sugar Land Texas. I love working out, playing video games and hanging out with friends.",
        commits: 0,
        issues: 0,
        tests: 12,
    },
    {
        name: "Taka Koutsomitopoulos",
        username: "takakoutso",
        email: "taka.koutso@gmail.com",
        picture: TakaKoutsomitopoulosImg,
        role: "Front End & Phase II Leader",
        bio: "Hi, I'm a 3rd year BS/MS CS Student. I'm originally from northern New Jersey, but I've lived in Austin for 7 years now. In my free time, I listen to music, play games, and play sports with my friends.",
        commits: 0,
        issues: 0,
        tests: 11,
    }
]

/*
 * JS object to hold data about all the tools used
 */
const toolsUsed = [
    {
        name: "React",
        link: "https://reactjs.org/",
        img: reactIMG,
        description: "We utilized React Router to link pages and provide navigation through the website.",
    },
    {
        name: "React Router",
        link: "https://reactrouter.com/",
        img: reactrouterIMG,
        description: "We utilized React Router to link pages and provide navigation through the website.",
    },
    {
        name: "React Bootstrap",
        link: "https://react-bootstrap.github.io/",
        img: reactbootstrapIMG,
        description: "We utilized React Bootstrap to refer to built in components.",
    },
    {
        name: "AWS Amplify",
        link: "https://aws.amazon.com/amplify/",
        img: awsIMG,
        description: "We utilized AWS Amplify to build and host the website.",
    },
    {
        name: "GitLab",
        link: "https://about.gitlab.com/",
        img: gitlabIMG,
        description: "We utilized GitLab to store our files and keep track of tasks.",
    },
    {
        name: "Postman",
        link: "https://www.postman.com/",
        img: postmanIMG,
        description: "We utilized Postman to design and document a RESTful API for Give & Live.",
    },
    {
        name: "Namecheap",
        link: "https://www.namecheap.com/",
        img: namecheapIMG,
        description: "We utilized Namecheap to retrieve a domain name for Give & Live.",
    },
    {
        name: "Docker",
        link: "https://www.docker.com/",
        img: dockerIMG,
        description: "We utilized Docker to configure AWS Amplify.",
    },
    {
        name: "Jest",
        link: "https://jestjs.io/",
        img: jestIMG,
        description: "We utilized Jest to test our JavaScript files.",
    },
    {
        name: "Selenium",
        link: "https://www.selenium.dev/",
        img: seleniumIMG,
        description: "We utilized Selenium to test our GUI.",
    },
    {
        name: "Flask",
        link: "https://flask.palletsprojects.com/en/2.1.x/",
        img: flaskIMG,
        description: "We utilized Flask to route and set up our RESTful API.",
    },
    {
        name: "SQLAlchemy",
        link: "https://www.sqlalchemy.org/",
        img: sqlalchemyIMG,
        description: "We utilized SQLAlchemy to communicate with our database.",
    },
    {
        name: "Marshmallow",
        link: "https://marshmallow-sqlalchemy.readthedocs.io/en/latest/api_reference.html",
        img: marshmallowIMG,
        description: "We utilized Marshmallow to set up schemas for our API data.",
    },
    {
        name: "PostGreSQL",
        link: "https://www.postgresql.org/",
        img: postreSQLIMG,
        description: "We utilized PostGreSQL to set up a database containing data for our RESTful API.",
    }
]

/*
 * JS object to hold data about all the APIs 
 */
const apisUsed = [
    {
        name: "Charity Navigator",
        link: "https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1397",
        img: cnIMG,
        description: "API used for retrieving data related to the charity instances. We pulled the top charities with unique locations, specifically the city, state, rating, zip code, and classification.",
    },
    {
        name: "TicketMaster",
        link: "https://developer.ticketmaster.com/products-and-docs/apis/getting-started/",
        img: ticketmasterIMG,
        description: "API used to display event information for the event instances. Using the locations of charities, TicketMaster provides the top events in the charity's city for our Events model. We get the event name, date, time, status, genre, and minimum price.",
    },
    {
        name: "Road Goat",
        link: "https://www.roadgoat.com/business/cities-api",
        img: rgIMG,
        description: "API used to extract city information for the city instances. Location model uses the locations of the charities to obtain information about the city like GPS latitude, longitude, budget, population, and safety.",
    },
    {
        name: "Google Maps",
        link: "https://developers.google.com/maps/documentation",
        img: gmIMG,
        description: "API used for the Google Maps user interface on the instance pages. Using the location of each charity, we use the Google Maps API to get geolocated data, the user locations, and the embedded map.",
    }
]

/*
 * JS object to hold data about our repo and postman documentation
 */
const ourLinks = [
    {
        name: "GitLab",
        link: "https://gitlab.com/giveandlive/giveandlive",
        img: gitlabIMG,
    },
    {
        name: "Postman",
        link: "https://documenter.getpostman.com/view/19786565/UVksKDPx",
        img: postmanIMG,
    }
]


// from https://stackoverflow.com/questions/43733179/gitlab-api-to-get-all-commits-of-a-specific-branch
/*
 * Helper class for interfacing with the Gitlab API to get info
 * about commits 
 */
class GitlabAPI {
    host = "https://gitlab.com/api/v4";
    project = null;
    constructor(host, project) {
      this.host = host;
      this.project = project;
      console.log(project);
    }
  
    async loadCommits(branch, commits = [], page = 1) {
      const url = `${this.host}/projects/${this.project}/repository/commits`;
  
      const response = await axios.get(url, {
        params: { all: "true", per_page: 100, page, ref_name: branch },
      });
  
      console.log(`Parse ${page} page, results: ${response.data.length} commits`);
  
      if (response.data.length > 0) {
        return await this.loadCommits(branch, commits.concat(response.data), page + 1);
      } else {
        return commits;
      }
    }
  
    async commits(branch) {
      try {
        const commits = await this.loadCommits(branch);
        return commits;
      } catch (error) {
        console.log(error);
      }
    }
}

/*
 * async function to get information about commits/tests/issues for members
 */
const fetchInfo = async () => {
    let commitCount = 0
    let issueCount = 0
    let testCount = 0

    // start off each member with 0 commits and 0 issues, since we could be re-updating
    // add their test count to the total test count since GitLab doesn't have that information
    teamMembers.forEach((member) => {
        testCount += member.tests
    })

    /*
    * loop over 4 pages of data from Gitlab API
    */
    for (let i = 1; i <= 4; i++) {
        let commitInfo = await fetch("https://gitlab.com/api/v4/projects/33893188/repository/commits?page=" + i + "&per_page=100")

        // wait for the json file then call a function to parse it
        await commitInfo.json().then(function (commitList) {
            // for each commit, determine who did it
            commitList.forEach((commit) => {
                // store the fields of the commit we need
                let {author_name} = commit
                // find the member whose name matches the commit
                //console.log(name);
                teamMembers.forEach((member) => {
                    // if the name matches a member's name, increment
                    // the members commit count
                    if (author_name === member.username || author_name === member.name) {
                        member.commits += 1
                        // HOW DO I BREAK OUT OF THIS FOR EACH LOOP???
                    }
                })
                commitCount += 1;
            })
        })
    }

    // get issue information from GitLab API
    let issueInfo = await fetch("https://gitlab.com/api/v4/projects/33893188/issues?per_page=100")

    // wait for json file and then call a function to parse it
    await issueInfo.json().then(function (issueList) {
        // for each issue, determine who made it
        issueList.forEach((issue) => {
            // store the issue's assignee
            const { assignees } = issue
            assignees.forEach((assignee) => {
                const {name, username} = assignee
                // if the member name matches, increment their issues count
                teamMembers.forEach((teamMember) => {
                    if (teamMember.username === username)
                        teamMember.issues += 1
                })
            })
            issueCount += 1
        })
    })

    return {
        totalCommits: commitCount,
        totalIssues: issueCount,
        totalTests: testCount,
        team: teamMembers
    }

}

const About = () => {
    // initialize data structures to store the gitlab api info in
    const [teamList, setTeam] = useState([])
    const [totalCommits, setTotalCommits] = useState(0)
    const [totalIssues, setTotalIssues] = useState(0)
    const [totalTests, setTotalTests] = useState(0)
    const [fetchedData, setFetchedData] = useState(false)

    useEffect(() => {
        // check to see if we have obtained info from gitlab api
        const fetchData = async () => {
            if (fetchedData === false) {
                const getInfo = await fetchInfo()
                setTotalCommits(getInfo.totalCommits)
                setTotalIssues(getInfo.totalIssues)
                setTotalTests(getInfo.totalTests)
                setTeam(getInfo.team)
                setFetchedData(true)
            }
        }
        fetchData()
    }, [teamList])

    // HTML output
    return (
        <div style={{ padding: '4vw' }}>
            {/* background info */}
            <h1 className="Title">About Give and Live</h1>
                <p className="aboutInfo">
                Give and Live provides users with an opportunity to search and identify U.S. charities based on location while also 
                informing the user of nearby events in the area. This connection allows the user to both give to a specific 
                community through charity support while also being able to live and experience the surrounding events and 
                amenities of the community overall. 
                </p>
            <h1 className="Title">Behind the Data</h1>
                <p className="aboutInfo">
                When reviewing the data of various charities, events, and locations, we were able to discover a few interesting 
                connections between them. We noticed that events that were located in cities with a higher budget (cost rating) 
                tended to have a more expensive ticket price. This is expected as these events are associated with the same costs 
                that vary with the cost of living in a city. We also noticed that charities were usually naturally linked to events 
                that were conveniently nearby which will ultimately provide users with ease of transportation.
                </p>
            {/* team member information */}
            <h1 className="Title">The Team</h1>
            {fetchedData ? (
                <Row xs={3} md={3} className="g-4">
                    {teamList.map((member) => {
                        const { name, username, email, picture, role, bio, commits, issues, tests } = member
                        return (
                            <Col key={member}>
                                <Card className = "memberCard">
                                    <Card.Header as="h5"> {name} </Card.Header>
                                    <Card.Img variant="top" src={picture} />
                                    <Card.Body style={{ backgroundColor: "#75C0FF", color: "white" }}>
                                        <ListGroup >
                                            <ListGroup.Item>{role}</ListGroup.Item>
                                            <ListGroup.Item>{bio}</ListGroup.Item>
                                            <ListGroup.Item># of Commits : {commits}</ListGroup.Item>
                                            <ListGroup.Item># of Issues : {issues}</ListGroup.Item>
                                            <ListGroup.Item># of Tests : {tests}</ListGroup.Item>
                                            <ListGroup.Item>Username : {username}</ListGroup.Item>
                                            <ListGroup.Item>Email : {email}</ListGroup.Item>
                                        </ListGroup>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            
                
            ) : (<div></div>)} {/* if got the data, render nothing*/}

            {/* information about gitlab repo */}
        {

            <div className="aboutCard">
                <Stack gap={1}>
                <Card className="bottomCard">
                        <Card.Title>GitLab Stats</Card.Title>
                        <Card.Body>
                        <Table className="statTable" striped bordered hover variant="dark">
                            <thead>
                                <tr>
                                <th>Total Commits</th>
                                <th>Total Issues</th>
                                <th>Total Tests</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td>{totalCommits}</td>
                                <td>{totalIssues}</td>
                                <td>{totalTests}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
                </Stack>
            </div>
        }
        {/* Info about the tools used */}
        {
            <div style={{ padding: '4vw' }}>
            <h1 className="Title">Tools Utilized</h1>
            <Row xs={3} md={3} className="g-4">
                    {toolsUsed.map((tool) => {
                        const { name, link, img, description} = tool
                        return (
                            <Col key={tool}>
                                <Card className = "toolCard" onClick={(e) => {e.preventDefault(); window.location.href=link;}}>
                                    <Card.Header as="h5"> {name} </Card.Header>
                                    <Card.Img variant="top" src={img} />
                                    <Card.Body style={{ backgroundColor: "#75C0FF", color: "white" }}>
                                        <ListGroup >
                                            <ListGroup.Item>{description}</ListGroup.Item>
                                        </ListGroup>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </div>
        }
        {/* info about APIs used */}
        {
            <div style={{ padding: '4vw' }}>
            <h1 className="Title">APIs Utilized</h1>
            <Row xs={1} md={4} className="g-4">
                    {apisUsed.map((api) => {
                        const { name, link, img, description} = api
                        return (
                            <Col key={api}>
                                <Card className = "toolCard" onClick={(e) => {e.preventDefault(); window.location.href=link;}}>
                                    <Card.Header as="h5"> {name} </Card.Header>
                                    <Card.Img variant="top" src={img} />
                                    <Card.Body style={{ backgroundColor: "#75C0FF", color: "white" }}>
                                        <ListGroup >
                                            <ListGroup.Item>{description}</ListGroup.Item>
                                        </ListGroup>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </div>
            
        }
        {/* links to repo and API docs */}
        {
            <div style={{ padding: '4vw' }}>
            <h1 className="Title">GitLab Repository and Postman API</h1>
            <Row xs={1} md={2} className="g-4">
                    {ourLinks.map((web) => {
                        const { name, link, img} = web
                        return (
                            <Col key={web}>
                                <Card className = "toolCard" onClick={(e) => {e.preventDefault(); window.location.href=link;}}>
                                    <Card.Header as="h5"> {name} </Card.Header>
                                    <Card.Img variant="top" src={img} width={300} height={300}/>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
            </div>    
        }
        </div>
    )
}

export default About;
