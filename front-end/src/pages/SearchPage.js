import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Table, Form, Stack} from 'react-bootstrap';
import axios from 'axios';
import Paginate from './Pagination.js'
import './SearchPage.css';
import './ModelPages.css';
import Highlighter from "react-highlight-words"

// Search Page Element
const SearchPage = () =>  {

    const page_size = 5; /* max number of elements per table  */

    const [currentPage, setCurrentPage] = useState(1); /* hook to keep track of page number */
    const [searchQuery, setSearchQuery] = useState(""); /* hook to keep track of search query */
    const [inputState, setInputState] = useState(0); /* hook to track whether we're new, loading, or searched */
    const [searchRes, setSearchRes] = useState(
                    {'charity': [], 'charity_count':0, 'event': [], 
                    'event_count':0, 'location': [], 'location_count':0}); /* hook to keep track of searched data */

    /* 
     * function called when pagination tile is clicked, changes
     * page hook and renders new page with correct data
     */
    const paginate = (num) => {
        setCurrentPage(num);
        getData(num, searchQuery);
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the requested search query and page
     */
    const getData = async(page, query) => {
        setInputState(1);

        // try to get the search query results from the API
        try {
            // generate URL based on search query, page, and page_size
            const url = 'https://api.giveandlive.me/search?search='+query+'&page='+page + '&per_page='+page_size; 
            const results = await axios.get(url);  
            setSearchRes(results.data);

            // if API request failed, change state to failed
            if (searchRes.length == 0) {
                setInputState(4);
            } else { // if no failed, change state to succeeded
                setInputState(2);
            }
        } catch (error) { // if we are unaable to get search results, change state to failed
            setInputState(4);
        }

    }

    /* 
     * function to handle when to search from the search box/button
     */
    function handleOnSubmit(e) {
        getData(currentPage, searchQuery);
        e.preventDefault();
    }

    // HTML output
    return (
        <div className='outer'>
            <center>
                <Form onSubmit={handleOnSubmit}>
                    <Form.Group className="searchBar" >
                        <Form.Label>Search for a Charity, Location, or Event</Form.Label>
                        <Form.Control type="text" placeholder="Enter Search Here" 
                            onChange={e => setSearchQuery(e.target.value)} />
                    </Form.Group>
                </Form>
                {/* if we are new to this page, render instructions */}
                {inputState==0 && <h3 style={{marginTop:'50px'}}>Enter a Search Query to start</h3>}
                {/* if we're waiting for API call, render loading */}
                {inputState==1 && <h3 style={{marginTop:'50px'}}>Loading...</h3>}
            </center>

            {/* if we've received data, render it */}
            {inputState==2 && <h3 style={{marginTop:'50px'}}>Got Data</h3> && 
            <div style={{marginTop:'15px'}}>

                {/* Render Charities Search Results */}

                <h3>Charities - {searchRes['charity_count']} entries found</h3>
                { (searchRes['charity'].length) == 0 && <h6> No data found</h6>}
                { (searchRes['charity'].length) > 0 && 
                <center>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                        <th>Charity Name</th>
                        <th>Rating</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip Code</th>
                        <th>Classification</th>
                        </tr>
                    </thead>

                    {/* Render row for each search result */}
                    <tbody>
                        {searchRes['charity'].map((charity, idx) => (
                            <tr key={idx} className='TableRow'
                                onClick={() => window.location.href = "/charities/"+charity['id']}>

                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight= {charity['name']}
                                    /></td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={"" + charity['overallRating']}
                                    /></td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={charity['city']}
                                    /></td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={charity['state']}
                                    /></td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={charity['zipCode']}
                                    /></td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={charity['classification']}
                                    /></td>
                            </tr>
                        ))}
                        
                    </tbody>
                </Table>
                </center>
                }

                {/* Render Events Search Results */}
            
                <h3>Events - {searchRes['event_count']} entries found</h3>
                { (searchRes['event'].length) == 0 && <h6> No data found</h6>}
                { (searchRes['event'].length) > 0 &&
                <center>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>Event Name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Status</th>
                            <th>Genre</th>
                            <th>Minimum Price</th>
                        </tr>
                    </thead>
                    {/* Render row for each search result */}
                    <tbody>
                        {searchRes['event'].map((event, idx) => (

                            <tr key={idx} className='TableRow'
                                onClick={() => window.location.href = "/events/"+event['id']}>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={event['name']}/>
                                    </td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={event['startDate']}/>
                                    </td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={event['startTime']}/>
                                </td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={event['status']}/>
                                </td>
                                <td><Highlighter
                                        searchWords={[searchQuery]}
                                        autoEscape={true}
                                        textToHighlight={event['genre']}/>
                                </td>
                                <td>{event['priceMin'] != -1 && event['priceMin']}
                                    {event['priceMin'] == -1 && "No Data"}
                                </td>
                            </tr>
                        ))}
                        
                    </tbody>
                </Table>
                </center>
                }

                {/* Render Locations Search Results */}

                <h3>Locations - {searchRes['location_count']} entries found</h3>
                { (searchRes['location'].length) == 0 && <h6> No data found</h6>}
                { (searchRes['location'].length) > 0 &&
                <center>
                <Table striped bordered hover >
                    <thead>
                        <tr>
                            <th>Location Name</th>
                            <th>GPS Latitude</th>
                            <th>GPS Longitude</th>
                            <th>Budget</th>
                            <th>Population</th>
                            <th>Safety</th>
                        </tr>
                    </thead>
                        
                    <tbody>
                        {searchRes['location'].map((location, idx) => (
                            <tr key={idx} className='TableRow'
                                onClick={() => window.location.href = "/locations/"+location['id']}>
                                <td>
                                    <Highlighter
                                            searchWords={[searchQuery]}
                                            autoEscape={true}
                                            textToHighlight={location['name']}/>
                                </td>
                                <td>
                                    <Highlighter
                                            searchWords={[searchQuery]}
                                            autoEscape={true}
                                            textToHighlight={"" + location['latitude']}/>
                                </td>
                                <td>
                                    <Highlighter
                                            searchWords={[searchQuery]}
                                            autoEscape={true}
                                            textToHighlight={"" + location['longitude']}/>
                                </td>
                                <td>
                                    <Highlighter
                                            searchWords={[searchQuery]}
                                            autoEscape={true}
                                            textToHighlight={"" + location['budget']}/>
                                </td>
                                <td>
                                    <Highlighter
                                            searchWords={[searchQuery]}
                                            autoEscape={true}
                                            textToHighlight={"" + location['population']}/>
                                </td>
                                <td>
                                    <Highlighter
                                            searchWords={[searchQuery]}
                                            autoEscape={true}
                                            textToHighlight={"" + location['safety']}/>
                                </td>
                            </tr>
                            ))}
                    </tbody>
                </Table>
                </center>    
                }
            </div>
            }

            {/* If there's data, then render the pagination component */}
            { inputState == 2 &&
                
                <Stack gap={0}>
                    <div className="center"><h5>Total Number of entries: {searchRes['charity_count'] + searchRes['event_count'] + searchRes['location_count']}</h5></div>
                    <div className="center"><h5>Page: {currentPage}/{Math.ceil(Math.max(searchRes['charity_count'], searchRes['event_count'], searchRes['location_count'])/page_size)}</h5></div>
                    <div className="center">
                        <Paginate totalItems={Math.max(searchRes['charity_count'], searchRes['event_count'], searchRes['location_count'])} itemsPerPage={page_size} paginate={paginate} />
                    </div>
                </Stack>   
            }
        </div>
       
    );

}

export default SearchPage