import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Table, Stack } from 'react-bootstrap';
import Paginate from './Pagination.js'
import axios from 'axios'
import './ModelPages.css';
import Select from 'react-select'
import {updateFilters} from './Filtering.js'
import Highlighter from "react-highlight-words"

// Charities model element
const Charities = ({ fetchPage }) => {
        
    const [currentPage, setCurrentPage] = useState(1); /* hook for page */
    const [charities_query, setCharities_query] = useState({'content': [], 'num':0}); /* hook for API result */
    const [gettingData, setGettingData] = useState(false); /* hook for tracking when waiting for API */
    const [filterChoices, setFilterChoices] = useState([]); /* hook for filter choices */
    const [filterQueries, setFilterQueries] = useState([]); /* hook for filter queries */
    const [searchQuery, setSearchQuery] = useState(""); /* hook for search query */
    const [sortquery, setSortquery] = useState(null); /* hook for sort query */
    const [filtering, setFiltering] = useState(null); /* hook for tracking when filtering */
    const [searching, setSearching] = useState(null); /* hook for tracking when searching */
    const [sorting, setSorting] = useState(null); /* hook for tracking when sorting */

    /* 
     * helper function to construct API call for other functions. Code also handles
     * discrepancies from previous page loads from unupdated hooks.
     */
    function getQuery(page, query, ignore) {
        let url = `https://api.giveandlive.me/charity`;
        url = url + `?page=${page}`

        // ignore handles overriding previous queries
        if (ignore == undefined) {
            ignore = ""
        }
        if (query == undefined) {
            query = "";
        }

        // if sort is up to date, then sort by the given query or the hook
        if (ignore !== 'sort') {
            if (query.includes("sort")) {
                url = url + '&' + query;
            }
            else if (sortquery !== "" && sortquery != null) {
                url = url + `&sort=${sortquery}`
            }
        }

        // if filter is up to date, then filter by the given query or the hook
        if (ignore !== 'filter') {
            if (query.includes("filter")) {
                url = url + '&' + query.replace("filter=", "");
            }
            else if (filtering != null && filtering !== "") {
                let updates = updateFilters(filtering[0], filtering[1], filterQueries, page)
                url = url + `&${updates.linkParams.split("&")[1]}`
            }
        }

        // if search is up to date, then search by the given query or the hook.
        if (ignore !== 'search') {
            if (query.includes("search")) {
                url = url + '&' + query;
            }
            else if (searching !== null && searching !== undefined && searching !== "") {
                if (searching.includes('search=')) {
                    url = url + `&${searching}`
                } else {
                    url = url + `&search=${searching}`
                }
            }
        }
        return url;
    }
    
    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the charity model page, specifically for searching.
     */
    const fetchCharities = async(page, query, ignore) =>  {
        setGettingData(true);
        setSearching(query);
        const url = getQuery(page, query);
        const res = await axios.get(url);
        setCharities_query(res.data);
        setGettingData(false);
    }
    
    /* 
     * function to call the giveandlive API endpoint corresponding to 
     * the charity model page, for general purpose.
     */
    const get_data = async(page, query, ignore) => {
        setGettingData(true);
        const url = getQuery(page, query, ignore);
        const c_q = await axios.get(url);
        setCharities_query(c_q.data);
        setGettingData(false);
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the filter choices, used for dynamically populating the filter box options.
     */
    const getFilterChoices = async() => {
        // call API endpoint for filter options for charity model page
        const classification = await axios.get('https://api.giveandlive.me/charity?classifications')
        const rating = await axios.get('https://api.giveandlive.me/charity?ratings')

        // retrieve just the classification and rating values and sort them
        let classificationCheck = classification.data.content.map((item) => item.classification)
        classificationCheck.sort(function (first, second) {return first - second})
        let ratingCheck = rating.data.content.map((item) => item.overallRating)
        ratingCheck.sort(function (first, second) {return first - second})

        // store the choices for use by React
        let classificationChoices = [...new Set(classificationCheck)]
        classificationChoices = classificationChoices.map((item) => {
            return {value: 'classification' + item, type: 'classification', label: item}
        })
        let ratingChoices = [...new Set(ratingCheck)]
        ratingChoices = ratingChoices.map((item) => {
            return {value: 'rating' + item, type: 'rating', label: item}
        })

        // store the default None values and store in the hook
        classificationChoices.unshift({value: 'classificationNone', type: 'classification', label: 'None'})
        ratingChoices.unshift({value: 'ratingNone' , type: 'rating', label: 'None'})
        setFilterChoices([classificationChoices, ratingChoices])
    }

    /* 
     * function to handle filtering selection and retrieve filtered data
     */
    const getFilteredPage = (page, type, choiceObj) => {
        setFiltering([type, choiceObj]);
        let updates = updateFilters(type, choiceObj, filterQueries, page)
        setFilterQueries([...updates.choices])
        let query = updates.linkParams.split("&")[1];
        let ignore = ""
        if (query == undefined) {
            ignore = 'filter'
            query = ""
        } else {
            query = 'filter=' + query;
        }
        get_data(page, query, ignore);
    }
    
    /* 
     * function to handle sorting selection and retrieve sorted data
     */
    const get_sorted_data = async(page, sort) => {
        setGettingData(true);
        setSorting(true);
        const url = getQuery(page, 'sort='+sort);
        const c_q = await axios.get(url);

        setCharities_query(c_q.data);
        setGettingData(false);
    }

    /* 
     * load default data and filter options on initial page load
     */
    useEffect(() => {
        get_data(1, "", "");
        getFilterChoices();
    }, [])

    /* 
     * function to paginate the model page, setting the page hook
     * and calling the correct function to reload the page
     */
    const paginate = (num) => {
        setCurrentPage(num);
        if (searching && (searching !== "")) {
            fetchCharities(num, searching);
        } else if (filtering) {
            getFilteredPage(num, filtering[0], filtering[1]);
        } else if (sorting) {
            get_sorted_data(num, sortquery);
        } else {
            get_data(num, "", "");
        }
    }

    /* 
     * handler function to simply reload the data with the correct query
     */
    const fetchSearchResults = () => {
        if (searchQuery!=null) {
            fetchCharities(1, `search=${searchQuery}`)
        } else {
            fetchCharities(1, '');
        }
    }

    /* 
     * handler function to search on Enter key press
     */
    const handleKeyDown = (event) => {
        if (event.key == "Enter") {
            fetchSearchResults()
        }
        
    }

    /* 
     * JS object to hold the sorting options
     */
    const features = [
        { label: "Name (A-Z)", value: 2 },
        { label: "Name (Z-A)", value: 1 },
        { label: "Rating (Desc)", value: 3 },
        { label: "Rating (Asc)", value: 4 },
        { label: "City (A-Z)", value: 6 },
        { label: "City (Z-A)", value: 5 },
        { label: "State (A-Z)", value: 8 },
        { label: "State (Z-A)", value: 7 },
        { label: "ZipCode (Desc)", value: 9 },
        { label: "ZipCode (Asc)", value: 10 },
        { label: "Classification (Desc)", value: 11 },
        { label: "Classification (Asc)", value: 12 },
    ];

    /* 
     * handler function to handle selection of a sort option, and
     * eventually retrieve the sorted data
     */
    const handleSelect=(e)=>{
            let sort = "";
            sort += e["label"].toLowerCase().split(" ")[0].trim()
            if (e["value"] % 2 == 1) {
                sort += "-descending"
            } else {
                sort += "-ascending"
            }
            setSortquery(sort);
            get_sorted_data(currentPage, sort);    
    }
    
    // HTML output
    return (
        <div className='outer'>
            <h1>Charities</h1>
            {/* Holds the options to change rendered data */}
            <div>
                {/* box for classification filter */}
                <div className='filterBox'>
                    <h6>Classification Filter</h6>
                    <Select options={filterChoices[0]} defaultValue="Name" isSearchable={true} 
                    onChange={(choice) => getFilteredPage(1, "classification", choice)} isClearable={true}/>
                </div>
                {/* box for rating filter */}
                <div className='filterBox'>
                    <h6>Rating Filter</h6>
                    <Select options={filterChoices[1]} defaultValue="Name" isSearchable={true} 
                    onChange={(choice) => getFilteredPage(1, "rating", choice)} isClearable={true}/>
                </div>
                {/* box for sorting */}
                <div className='filterBox'>
                    <h6>Sort</h6>
                    <Select id="sort" options={ features }
                            onChange={handleSelect}
                            onSelect={handleSelect}/>
                </div>
                {/* box for searching */}
                <div style={{ height:'100px', float:'left'}}>
                    <h6>Charities Search</h6>
                    <input style={{float:'left', paddingLeft:'15px', width: '200px', height: '38px'}} 
                        type="text" onChange={e => setSearchQuery(e.target.value)} onKeyPress={handleKeyDown}
                        placeholder="Enter query"/>
                    <Button style={{ backgroundColor: '#00008b', float:'left', marginLeft:'15px'}} 
                        variant="primary" type="submit" onClick={() => fetchSearchResults()}>
                        Submit
                    </Button>
                </div>
            </div>
            {/* handles the end of the float */}
            <div style={{clear:'both'}}></div>
            {/* Dynamically renders the current page of data */}
            {gettingData && <h3>Loading...</h3>}
            {!gettingData &&
            <Table striped bordered hover >
                <thead>
                    <tr>
                        <th>Charity Name</th>
                        <th>Rating</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip Code</th>
                        <th>Classification</th>
                    </tr>
                </thead>
                <tbody>
                    {/* for each charity object, render the following */}
                    {charities_query['content'].map((charity, idx) => (

                        <tr key={idx} className='TableRow'
                            onClick={() => window.location.href = "/charities/"+charity['id']}>
                            <td><Highlighter
                                    searchWords={[searchQuery]}
                                    autoEscape={true}
                                    textToHighlight={charity['name']}/></td>
                            <td>{charity['overallRating']}</td>
                            <td><Highlighter
                                    searchWords={[searchQuery]}
                                    autoEscape={true}
                                    textToHighlight={charity['city']}/></td>
                            <td><Highlighter
                                    searchWords={[searchQuery]}
                                    autoEscape={true}
                                    textToHighlight={charity['state']}/></td>
                            <td>{charity['zipCode']}</td>
                            <td>{charity['classification']}</td>
                        </tr>
                    ))}
                </tbody>
            </Table>
            }
            {/* Render information about this page of data */}
            <Stack gap={0}>
                <div className="center"><h5>Number of entries: {charities_query['num']}</h5></div>
                <div className="center"><h5>Page: {currentPage}/{Math.ceil(charities_query['num']/20)}</h5></div>
                <div className="center"><Paginate totalItems={charities_query['num']} itemsPerPage={20} paginate={paginate} /></div>
            </Stack>
        </div>
    );
};

export default Charities

