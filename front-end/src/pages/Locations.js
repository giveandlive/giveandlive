import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import { Button, Table, Stack } from 'react-bootstrap';
import Paginate from './Pagination.js'
import axios from 'axios'
import './ModelPages.css';
import Select from 'react-select'
import { updateFilters } from './Filtering.js'
import Highlighter from "react-highlight-words"

const Locations = ({ fetchPage }) => {

    const [currentPage, setCurrentPage] = useState(1); /* hook for page */
    const [locations_query, setLocations_query] = useState({'content': [], 'num':0}); /* hook for API result */
    const [gettingData, setGettingData] = useState(false); /* hook for tracking when waiting for API */
    const [filterChoices, setFilterChoices] = useState([]); /* hook for filter choices */
    const [filterQueries, setFilterQueries] = useState([]); /* hook for filter queries */
    const [searchQuery, setSearchQuery] = useState(""); /* hook for search query */
    const [sortquery, setSortquery] = useState(""); /* hook for sort query */
    const [filtering, setFiltering] = useState(null); /* hook for tracking when filtering */
    const [searching, setSearching] = useState(null); /* hook for tracking when searching */
    const [sorting, setSorting] = useState(null); /* hook for tracking when sorting */

    /* 
     * helper function to construct API call for other functions. Code also handles
     * discrepancies from previous page loads from unupdated hooks.
     */
    function getQuery(page, query, ignore) {
        let url = `https://api.giveandlive.me/location`;
        url = url + `?page=${page}`
        
        // ignore handles overriding previous queries
        if (ignore == undefined) {
            ignore = ""
        }
        if (query == undefined) {
            query = "";
        }

        // if sort is up to date, then sort by the given query or the hook
        if (ignore !== 'sort') {
            if (query.includes("sort")) {
                url = url + '&' + query;
            }
            else if (sortquery !== "" && sortquery != null) {
                url = url + `&sort=${sortquery}`
            }
        }

        // if filter is up to date, then filter by the given query or the hook
        if (ignore !== 'filter') {
            if (query.includes("filter")) {
                url = url + '&' + query.replace("filter=", "");
            }
            else if (filtering != null && filtering !== "") {
                let updates = updateFilters(filtering[0], filtering[1], filterQueries, page)
                url = url + `&${updates.linkParams.split("&")[1]}`
            }
        }

        // if search is up to date, then search by the given query or the hook.
        if (ignore !== 'search') {
            if (query.includes("search")) {
                url = url + '&' + query;
            }
            else if (searching !== null && searching !== undefined && searching !== "") {
                if (searching.includes('search=')) {
                    url = url + `&${searching}`
                } else {
                    url = url + `&search=${searching}`
                }
            }
        }
        return url;
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the location model page, specifically for searching.
     */
    const fetchLocations = async(page, query, ignore) =>  {
        setGettingData(true);
        setSearching(query);
        const url = getQuery(page, query);
        const res = await axios.get(url);
        setLocations_query(res.data);
        setGettingData(false)
    }

    /* 
     * function to call the giveandlive API endpoint corresponding to 
     * the location model page, for general purpose.
     */
    const get_data = async(page, query, ignore) => {
        setGettingData(true);
        const url = getQuery(page, query, ignore);
        const c_q = await axios.get(url);
        setLocations_query(c_q.data);
        setGettingData(false);
    }

    /* 
     * async function to call the giveandlive API endpoint corresponding to 
     * the filter choices, used for dynamically populating the filter box options.
     */
    const getFilterChoices = async() => {
        // call API endpoint for filter options for location model page
        const budget = await axios.get('https://api.giveandlive.me/location?budgets')
        const safety = await axios.get('https://api.giveandlive.me/location?safeties')
        
        // retrieve just the budget and safety values and sort them
        let budgetCheck = budget.data.content.map((item) => item.budget)
        budgetCheck.sort(function (first, second) {return first - second})
        let safetyCheck = safety.data.content.map((item) => item.safety)
        safetyCheck.sort(function (first, second) {return first - second})
        
        // store the choices for use by React
        let budgetChoices = [...new Set(budgetCheck)]
        budgetChoices = budgetChoices.map((item) => {
            return {value: 'budget' + item, type: 'budget', label: item}
        })
        let safetyChoices = [...new Set(safetyCheck)]
        safetyChoices = safetyChoices.map((item) => {
            return {value: 'safety' + item, type: 'safety', label: item}
        })

        // store the default None values and store in the hook
        budgetChoices.unshift({value: 'budgetNone', type: 'budget', label: 'None'})
        safetyChoices.unshift({value: 'safetyNone' , type: 'safety', label: 'None'})
        setFilterChoices([budgetChoices, safetyChoices])
    }

    /* 
     * function to handle filtering selection and retrieve filtered data
     */
    const getFilteredPage = (page, type, choiceObj) => {
        setFiltering([type, choiceObj]);
        let updates = updateFilters(type, choiceObj, filterQueries, page)
        setFilterQueries([...updates.choices])
        let query = updates.linkParams.split("&")[1];
        let ignore = ""
        if (query == undefined) {
            ignore = 'filter'
            query = ""
        } else {
            query = 'filter=' + query;
        }
        get_data(page, query, ignore);
    }

    /* 
     * function to handle sorting selection and retrieve sorted data
     */
    const get_sorted_data = async(page, sort) => {
        setGettingData(true);
        setSorting(true);
        const url = getQuery(page, 'sort='+sort);
        const c_q = await axios.get(url);

        setLocations_query(c_q.data);
        setGettingData(false);
    }

    /* 
     * load default data and filter options on initial page load
     */
    useEffect(() => {
        get_data(1, "", "");
        getFilterChoices();
    }, [])

    /* 
     * function to paginate the model page, setting the page hook
     * and calling the correct function to reload the page
     */
    const paginate = (num) => {
        setCurrentPage(num);
        if (searching) {
            fetchLocations(num, searching);
        } else if (filtering) {
            getFilteredPage(num, filtering[0], filtering[1]);
        } else if (sorting) {
            get_sorted_data(num, sortquery);
        } else {
            get_data(num);
        }
    }

    /* 
     * handler function to simply reload the data with the correct query
     */
    const fetchSearchResults = () => {
        if (searchQuery!=null) {
            fetchLocations(1, `search=${searchQuery}`)
        } else {
            fetchLocations(1, '');
        }
    }

    /* 
     * handler function to search on Enter key press
     */
    const handleKeyDown = (event) => {
        if (event.key == "Enter") {
            fetchSearchResults()
        }
    }

    /* 
     * JS object to hold the sorting options
     */
    const features = [
        { label: "Name (A-Z)", value: 2 },
        { label: "Name (Z-A)", value: 1 },
        { label: "Latitude (Desc)", value: 3 },
        { label: "Latitude (Asc)", value: 4 },
        { label: "Longitude (Asc)", value: 6 },
        { label: "Longitude (Desc)", value: 5 },
        { label: "Budget (Asc)", value: 8 },
        { label: "Budget (Desc)", value: 7 },
        { label: "Population (Desc)", value: 9 },
        { label: "Population (Asc)", value: 10 },
        { label: "Safety (Desc)", value: 11 },
        { label: "Safety (Asc)", value: 12 },
    ];

    /* 
     * handler function to handle selection of a sort option, and
     * eventually retrieve the sorted data
     */
    const handleSelect=(e)=>{
        let sort = "";
        sort += e["label"].toLowerCase().split(" ")[0].trim()
        if (e["value"] % 2 == 1) {
            sort += "-descending"
        } else {
            sort += "-ascending"
        }
        setSortquery(sort);
        get_sorted_data(currentPage, sort);    
    }

    // HTML output
    return (
        <div className='outer'>
        <h1>Locations</h1>
        {/* Holds the options to change rendered data */}
        <div>
            {/* box for budget filter */}
            <div style={{ width:"200px", height:'100px', float:'left', marginRight: '15px'}}>
            <h6>Filter by Budget</h6>
                <Select options={filterChoices[0]} defaultValue="Name" isSearchable={true} 
                onChange={(choice) => getFilteredPage(1, "budget", choice)} isClearable={true}/>
            </div>
            {/* box for safety filter */}
            <div style={{ width:"200px", marginRight: '15px', height:'100px', float:'left'}}>
            <h6>Filter by Safety</h6>
                <Select options={filterChoices[1]} defaultValue="Name" isSearchable={true} 
                onChange={(choice) => getFilteredPage(1, "safety", choice)} isClearable={true}/>
            </div>
            {/* box for sorting */}
            <div style={{ width:"200px", height:'100px', float:'left', marginRight: '15px'}}>
                <h6>Sort</h6>
                    <Select options={ features }
                            onChange={handleSelect}
                            onSelect={handleSelect}/>
            </div>
            {/* box for searching */}
            <div style={{ height:'100px', float:'left'}}>
                <h6>Locations Search</h6>
                <input style={{float:'left', paddingLeft:'15px', width: '200px', height: '38px'}} 
                    type="text" onChange={e => setSearchQuery(e.target.value)} onKeyPress={handleKeyDown}
                    placeholder="Enter query" />
                <Button style={{ backgroundColor: '#00008b', float:'left', marginLeft:'15px'}} 
                    variant="primary" type="submit" onClick={() => fetchSearchResults()}>
                    Submit
                </Button>
            </div>        
        </div>
        {/* handles the end of the float */}
        <div style={{clear:'both'}}></div>
        {/* Dynamically renders the current page of data */}
        {gettingData && <h3>Loading...</h3>}
        {!gettingData &&
         <Table striped bordered hover>
         <thead>
             <tr>
             <th>Location Name</th>
             <th>GPS Latitude</th>
             <th>GPS Longitude</th>
             <th>Budget</th>
             <th>Population</th>
             <th>Safety</th>
             </tr>
         </thead>
         <tbody>
            {/* for each location object, render the following */}
            {locations_query['content'].map((location, idx) => (
            <tr key={idx} className='TableRow'
                 onClick={() => window.location.href = "/locations/"+location['id']}>
                <td><Highlighter
                        searchWords={[searchQuery]}
                        autoEscape={true}
                        textToHighlight={location['name']}/>
                </td>
            <td>{location['latitude']}</td>
            <td>{location['longitude']}</td>
            <td>{location['budget']}</td>
            <td>{location['population']}</td>
            <td>{location['safety']}</td>
            </tr>
            ))}
         </tbody>
         </Table>
        }
        {/* Render information about this page of data */}
        <Stack gap={0}>
            <div className="center"><h5>Number of entries: {locations_query['num']}</h5></div>
            <div className="center"><h5>Page: {currentPage}/{Math.ceil(locations_query['num']/20)}</h5></div>
            <div className="center">
                <Paginate totalItems={locations_query['num']} itemsPerPage={20} paginate={paginate} />
            </div>
        </Stack>   
       </div>
    );
}
  

export default Locations