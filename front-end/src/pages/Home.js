import React from 'react';
import { Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';
import { Card, Button } from 'react-bootstrap';
import Charities from './Charities'
import './Home.css';

// Home element for splash page
const Home = () =>  {
    return (
        <header style={{margin:"10px", width:"100%"}}>
            {/* main splash image */}
            <center><img 
                src="https://d20umu42aunjpx.cloudfront.net/landingpages/volunteer-og-image.png"
                style={{width: "100%"}}/>
            </center>

            {/* 3 cards for the model links */}
            <center style={{width:"100%"}}>
            <div className="cardDiv">
                <Card style={{ width:"33%" }}>
                    <Card.Img style={{ height:"18.25rem" }} variant="top" src="https://www.treepeople.org/wp-content/uploads/2020/12/Header-Get-Involved.jpg" />
                    <Card.Body>
                        <Card.Title>Charities</Card.Title>
                        <Card.Text>Search for a U.S. charity to support here!</Card.Text>
                        <Link to="/charities">
                            <Button variant="primary" src={Charities}> Charities </Button>
                        </Link>
                    </Card.Body>
                    {/* </Link> */}
                </Card>

                <Card style={{ width:"33%" }}>
                    <Card.Img style={{ height:"18.25rem" }} variant="top" src="https://assets.simpleviewinc.com/simpleview/image/upload/c_fill,h_667,q_75,w_1000/v1/clients/austin/10_05_Lifestyle_by_Sara_Marjorie_Strick_1_6ce98fe6-8836-4a88-b4e2-caed00b952e4.jpg" />
                    <Card.Body>
                        <Card.Title>Events</Card.Title>
                        <Card.Text>Take a look at various events around the U.S!</Card.Text>
                        <Link to="/events">
                            <Button variant="primary"> Events </Button>
                        </Link>
                    </Card.Body>
                </Card>

                <Card style={{ width:"33%" }}>
                    <Card.Img style={{ height:"18.25rem" }} variant="top" src="https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/gettyimages-688899881-1519413300.jpg" />
                    <Card.Body>
                        <Card.Title>Locations</Card.Title>
                        <Card.Text>Explore cities with charities and events!</Card.Text>
                        <Link to="/locations">
                            <Button variant="primary"> Locations </Button>
                        </Link>
                    </Card.Body>
                </Card>
            </div>
            </center>
        </header>
    );
}

export default Home;
