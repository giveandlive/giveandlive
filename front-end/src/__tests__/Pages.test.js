import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import About from "../pages/About"
import Charities from "../pages/Charities"
import Events from "../pages/Events"
import Locations from "../pages/Locations"
import CharityInstance from "../pages/CharityInstance"
import EventInstance from "../pages/EventInstance"
import LocationInstance from "../pages/LocationInstance"
import SearchPage from "../pages/SearchPage"

configure({ adapter: new Adapter() })

describe("Render Main Pages", () => {
	test("Charities 1", () => {
		const testCharities1 = shallow(<Charities />)
		expect(testCharities1).not.toBeUndefined()
		expect(testCharities1).toHaveLength(1)
	})

	test("Charities 2", () => {
		const testCharities2 = shallow(<Charities />)
		expect(testCharities2).toMatchSnapshot()
	})

	test("Events 1", () => {
		const testEvents1 = shallow(<Events />)
		expect(testEvents1).not.toBeUndefined()
		expect(testEvents1).toHaveLength(1)
	})

	test("Events 2", () => {
		const testEvents2 = shallow(<Events />)
		expect(testEvents2).toMatchSnapshot()
	})

	test("Locations 1", () => {
		const testLocations1 = shallow(<Locations />)
		expect(testLocations1).not.toBeUndefined()
		expect(testLocations1).toHaveLength(1)
	})

    test("Locations 2", () => {
		const testLocations2 = shallow(<Locations />)
		expect(testLocations2).toMatchSnapshot()
	})

	test("About 1", () => {
		const testAbout1 = shallow(<About />)
		expect(testAbout1).not.toBeUndefined()
		expect(testAbout1).toHaveLength(1)
	})

    test("About 2", () => {
		const testAbout2 = shallow(<About />)
		expect(testAbout2).toMatchSnapshot()
	})
})

describe("Render Instance Pages", () => {
	test("Charity Instance 1", () => {
		const testCharityInstance1 = shallow(<CharityInstance />)
		expect(testCharityInstance1).not.toBeUndefined()
		expect(testCharityInstance1).toHaveLength(1)
	})

	test("Charity Instance 2", () => {
		const testCharityInstance2 = shallow(<CharityInstance />)
		expect(testCharityInstance2).toMatchSnapshot()
	})

	test("Event Instance 1", () => {
		const testEventInstance1 = shallow(<EventInstance />)
		expect(testEventInstance1).not.toBeUndefined()
		expect(testEventInstance1).toHaveLength(1)
	})

	test("Event Instance 2", () => {
		const testEventInstance2 = shallow(<EventInstance />)
		expect(testEventInstance2).toMatchSnapshot()
	})

	test("Location Instance 1", () => {
		const testLocationInstance1 = shallow(<LocationInstance />)
		expect(testLocationInstance1).not.toBeUndefined()
		expect(testLocationInstance1).toHaveLength(1)
	})

    test("Location Instance 2", () => {
		const testLocationInstance2 = shallow(<LocationInstance />)
		expect(testLocationInstance2).toMatchSnapshot()
	})
})

describe("Searching, Sorting, and Filtering", () => {
	test("Overall Searching", () => {
		const testCharitySearch = shallow(<SearchPage />)
		const check = testCharitySearch.find('Search')
		expect(check).toBeTruthy()
	})

	test("Charity Searching", () => {
		const testCharitySearch = shallow(<Charities />)
		const check = testCharitySearch.find('Search')
		expect(check).toBeTruthy()
	})

	test("Charity Sorting", () => {
		const testCharitySort = shallow(<Charities />)
		const check = testCharitySort.find('Sort')
		expect(check).toBeTruthy()
	})

	test("Charity Filtering", () => {
		const testCharityFilter = shallow(<Charities />)
		const check = testCharityFilter.find('Filter')
		expect(check).toBeTruthy()
	})

	test("Event Searching", () => {
		const testEventSearch = shallow(<Events />)
		const check = testEventSearch.find('Search')
		expect(check).toBeTruthy()
	})

	test("Event Sorting", () => {
		const testEventSort = shallow(<Events />)
		const check = testEventSort.find('Sort')
		expect(check).toBeTruthy()
	})

	test("Event Filtering", () => {
		const testEventFilter = shallow(<Events />)
		const check = testEventFilter.find('Filter')
		expect(check).toBeTruthy()
	})

	test("Location Searching", () => {
		const testLocationSearch = shallow(<Locations />)
		const check = testLocationSearch.find('Search')
		expect(check).toBeTruthy()
	})

	test("Location Sorting", () => {
		const testLocationSort = shallow(<Locations />)
		const check = testLocationSort.find('Sort')
		expect(check).toBeTruthy()
	})

	test("Location Filtering", () => {
		const testLocationFilter = shallow(<Locations />)
		const check = testLocationFilter.find('Filter')
		expect(check).toBeTruthy()
	})
})
