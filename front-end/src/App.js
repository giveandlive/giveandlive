import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import Home from "./pages/Home";
import { Navbar, Nav, Container} from 'react-bootstrap';
import Charities from './pages/Charities';
import Events from './pages/Events';
import About from './pages/About';
import Locations from './pages/Locations'
import CharityInstance from './pages/CharityInstance';
import { BrowserRouter as Router, Routes, Route} from "react-router-dom"
import LocationInstance from './pages/LocationInstance';
import EventInstance from './pages/EventInstance';
import SearchPage from './pages/SearchPage';
import Visualizations from './pages/Visualizations';


// creates routing for website and links to all the pages
function App() {
  return (
    <div >
        <div>
        <Router>
          {/* navigation bar for whole website, links to 
            * model pages, search, main page, about, visualizations
            */}
          <Navbar bg="black" variant="dark">
          <Container>
            <Navbar.Brand href="/home">Give & Live</Navbar.Brand>
            <Nav className="me-auto">
              <Nav.Link href="/charities">Charities</Nav.Link>
              <Nav.Link href="/events">Events</Nav.Link>
              <Nav.Link href="/locations">Locations</Nav.Link>
              <Nav.Link href="/about">About</Nav.Link>
              <Nav.Link href="/visualizations">Visualizations</Nav.Link>
              <Nav.Link href="/search">Search</Nav.Link>
            </Nav>
          </Container>
          </Navbar>

          {/* establish routes to all pages in website */}
          <Routes>
              <Route path="/charities" element={<Charities/>} />
              <Route path="/charities/:charityId" element={<CharityInstance/>} />
              <Route path="/events" element={<Events/>} />
              <Route path="/events/:eventId" element={<EventInstance/>} />
              <Route path="/locations" element={<Locations/>} />
              <Route path="/locations/:locationId" element={<LocationInstance/>} />
              <Route path="/about" element={<About/>} />
              <Route path="home" element={<Home/>}/>
              <Route path="/visualizations" element={<Visualizations/>}/>
              <Route path="/search" element={<SearchPage/>}/>
              <Route path="*" element={<Home/>}/>
          </Routes>
        </Router>
       
       </div>
       </div>
  );
}

export default App;
