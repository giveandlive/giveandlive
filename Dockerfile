FROM node:14

COPY . /site

WORKDIR /site

RUN cd ./front-end
RUN npm install
RUN npm run build

WORKDIR /site/front-end

CMD ["npm", "start"]
