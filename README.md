# Give & Live

## Team Members

- Bruce Nguyen, bdn469, brucedatnguyen
- Malithy Wimalasooriya, mw38399, malithyw
- Sahran Hashim, shh738, sahranhashim99
- Taka Koutsomitopoulos, vtk97, takakoutso
- Varun Nayak, vn4639, varunn5

## Git SHA

- Phase 1: 00740409473d9e27006786da185950dd06741b28
- Phase 2: 260215fb5f072e5af9e076c5f296f2cf01db9476
- Phase 3: c928ed13f0e1e55b4e508202ad63ef33461d6318
- Phase 4: 89844f4761dbe22b0824e91c4bb3a84f95d38df5

## Project Leader

- Phase 1: Bruce Nguyen
- Phase 2: Taka Koutsomitopoulos
- Phase 3: Sahran Hashim
- Phase 4: Malithy Wimalasooriya

## GitLab Pipelines

- https://gitlab.com/giveandlive/giveandlive/-/pipelines

## Website Link

- https://giveandlive.me

## RESTful API Link

- https://api.giveandlive.me/

## Phase 1 Estimated Completion Time

- Bruce Nguyen: 25 hours
- Malithy Wimalasooriya: 25 hours
- Sahran Hashim: 25 hours
- Taka Koutsomitopoulos: 25 hours
- Varun Nayak: 25 hours

## Phase 1 Actual Completion Time

- Bruce Nguyen: 33 hours
- Malithy Wimalasooriya: 37 hours
- Sahran Hashim: 34 hours
- Taka Koutsomitopoulos: 32 hours
- Varun Nayak: 31 hours

## Phase 2 Estimated Completion Time

- Bruce Nguyen: 40
- Malithy Wimalasooriya: 40
- Sahran Hashim: 40
- Taka Koutsomitopoulos: 40
- Varun Nayak: 40

## Phase 2 Actual Completion Time

- Bruce Nguyen: 42
- Malithy Wimalasooriya: 41
- Sahran Hashim: 40
- Taka Koutsomitopoulos: 43
- Varun Nayak: 40

## Phase 3 Estimated Completion Time

- Bruce Nguyen: 40
- Malithy Wimalasooriya: 40
- Sahran Hashim: 40
- Taka Koutsomitopoulos: 40
- Varun Nayak: 40

## Phase 3 Actual Completion Time

- Bruce Nguyen: 43
- Malithy Wimalasooriya: 42
- Sahran Hashim: 41
- Taka Koutsomitopoulos: 44
- Varun Nayak: 41

## Phase 4 Estimated Completion Time

- Bruce Nguyen: 20
- Malithy Wimalasooriya: 20
- Sahran Hashim: 20
- Taka Koutsomitopoulos: 20
- Varun Nayak: 20

## Phase 4 Actual Completion Time

- Bruce Nguyen: 22
- Malithy Wimalasooriya: 23
- Sahran Hashim: 21
- Taka Koutsomitopoulos: 21
- Varun Nayak: 20


## Comments

- We are thankful to have been able to learn about so many tools while working on this project.
- We referenced Adopt a Pet's repository for our About page, Pagination.js, CI, and backend files.
	- https://gitlab.com/10am-group-8/adopt-a-pet
- We referenced Going for Gold's repository for our app.py file.
	- https://gitlab.com/rubenhambardzumyan/cs-373-10am-group-10/
- We referenced Texas Votes' repository for our query_helpers.py, db.py, db_push.py, models.py, and app.py files.
	- https://gitlab.com/forbesye/fitsbits/
- We referenced Flight Right's repository for our gui_test.py and CI files.
	- https://gitlab.com/flight-right/cs373_project
- We referenced Jefferson Ye's CS 373 Supplemental Information repository for supplemental backend files.
	- https://github.com/forbesye/cs373
- Thank you!

## RFP

- Group Time and Number: 11AM Group 6

- Project Name: Give & Live
- Team Member Names: Taka Koutsomitopoulos, Malithy Wimalasooriya, Bruce Nguyen, Varun Nayak, Sahran Hashim
- Repository: https://gitlab.com/giveandlive/giveandlive
- Project Overview: **We are providing a website that will let people select U.S. charities based on location, and also provide information about events in that area. The user is engaging with a community by giving (charity) and living (local events).**

- API URLs
	- Charity Navigator - API used for retrieving data related to the charity instances. 
		- https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1397
	- TicketMaster - API used to display event information for the event instances. 
		- https://developer.ticketmaster.com/products-and-docs/apis/getting-started/
	- Road Goat - API used to extract city information for the city instances. 
		- https://www.roadgoat.com/business/cities-api
	- Google Maps - API used for the Google Maps user interface on the instance pages. 
		- https://developers.google.com/maps/documentation
- Models
	- Charities
		- Estimate number of instances: ~200
		- Sortable Attributes:
			- id 
			- primary_key 
			- charityEIN
    		- overallRating
    		- financialRating	
		- Searchable Attributes
            - country
            - mission
    		- url
    		- tagLine
    		- state
    		- zipCode
    		- classification
    		- event 
    		- location
    		- city
            - name
    		- address
		- Connection between Models
			- City/Town the Charity is in
			- Events nearby the charity in distance/time
		- Media
			- Google Maps view
            - Street View Imagery
            - Logo of the Charity
            - Image published by Charity
            - Text to describe attributes
	- Locations
		- Estimate Number of instances: ~200
		- Sortable Attributes
    		- id
    		- latitude
    		- longitude
    		- rating
    		- population 
		- Searchable Attributes:
    		- name 
    		- city
    		- state
    		- population
    		- budget 
    		- budget_text 
			- safety 
    		- safety_text 
    		- covid 
    		- covid_text 
    		- photo_url 
    		- charity_info 
    		- event_info
		- Connection between Models
			- Charities that are hosted in the city
			- Events that are hosted in the city
		- Media
			- Google Maps view
            - Street View Imagery
            - Text to describe attributes
	- Events
		- Estimate Number of instances: ~200
		- Sortable Attributes:
    		- id
			- date
			- startDate
			- startTime
			- priceMin
			- priceMax
			- status
			- ticketLimit
		- Searchable Attributes:
            - city
            - state
            - image
            - name
            - genre
            - info
            - promoter
            - twitter
            - venue
            - pleaseNote
            - segment
            - status
            - address
            - venue
            - charity_info
            - location_info
		- Connection between Models
			- Charity events that are happening simultaneously
			- Location/City that the event is hosted in
		- Media
			- Image of the event
			- Social media embeds
            - Google maps embed of the location/directions
- Questions
	- What nearby events can I enjoy after volunteering at this charity?
    - What are some events I can attend around this area?
    - What are some of the best charities for me?
